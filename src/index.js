import React from 'react';
import ReactDOM from 'react-dom';
import App from './_app/App';
import * as serviceWorker from './serviceWorker';
import 'react-chatbox-component/dist/style.css';

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
