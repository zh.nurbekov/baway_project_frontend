import React from 'react';
import { Provider } from 'react-redux';
import { store, history } from '../_helpers/store';
import LocaleProvider from '../components/localeProvider/LocaleProvider';
import StyleProvider from '../components/styleProvider/StyleProvider';
import Root from './Root';
import { Router } from 'react-router-dom';
import { UseLocalSecureStorage } from '../utils/UseLocalSecureStorage';

export const localSecureStorage = UseLocalSecureStorage();

export default function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <LocaleProvider>
          <StyleProvider>
            <Root />
          </StyleProvider>
        </LocaleProvider>
      </Router>
    </Provider>
  );
}
