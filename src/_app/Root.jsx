import React, { useEffect } from 'react';
import { useLocation, withRouter } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import routers from '../_helpers/routers';
import ProtectedRoute from '../_ui/ProtectedRoute';
import { Route, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import ProgressBar from '../components/progressBar/ProgressBar';
import { handbookModul } from '../components/handbook/HandbookDucks';
import HeaderMenu from '../components/menu/HeaderMenu';
import Error404 from '../components/errors/Error404';
import { isEmpty } from 'lodash';
import { localSecureStorage } from './App';
import {loadCheckMessage, loadNotifications} from '../pages/message/MessageDucks';
import { getRole } from '../pages/usermanage/roles/RolesDucks';
import Footer from '../components/Footer/Footer';
import Notifications from "../pages/Notifications";

const Root = () => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const handbook = useSelector(state => state[handbookModul]);
  let user = localSecureStorage.getItem('user');

  useEffect(() => {
    if (user) {
      dispatch(loadCheckMessage(user.id));
      dispatch(loadNotifications(user.id));
      dispatch(getRole(user.role));
    }
  }, [pathname]);

  localSecureStorage.setItem('lastSign');

  return (
    <>
      <NotificationContainer />
      <Notifications/>
      {!isEmpty(handbook) ? (
        <div
          style={{
            backgroundColor: '#f0f2f5',
            height: '100%',
            display: 'flex',
            color: '#a8a8a8'
          }}
        >
          <Switch>
            {routers
              .filter(item => item.hideSideBar)
              .map(route => (
                <ProtectedRoute key={route.path} {...route} />
              ))}
            <Route
              render={() => (
                <div style={{ width: '100%' }}>
                  <HeaderMenu>
                    <Switch>
                      {routers
                        .filter(item => !item.hideSideBar)
                        .map(route => (
                          <ProtectedRoute key={route.path} {...route} />
                        ))}
                      <Route render={() => <Error404 />} />
                    </Switch>
                  </HeaderMenu>
                  <Footer />
                </div>
              )}
            />
          </Switch>
        </div>
      ) : null}
      <ProgressBar />
    </>
  );
};

export default withRouter(Root);
