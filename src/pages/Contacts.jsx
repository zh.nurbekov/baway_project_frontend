import React from 'react';
import { Paper } from '@material-ui/core';
import { Divider } from 'antd';

export default function Contacts() {
  return (
    <div style={{ width: '100%', color: 'black', textAlign: 'center' }}>
      <div className="p4">
        <Paper style={{ padding: 20 }}>
          <div>Контакты</div>
          <div className="flex justify-between items-center">
            <div>ПИШИТЕ</div>
            <div className="fs-13">ukural.ru@gmail.com</div>
          </div>
          <Divider />
          <div className="flex justify-between items-center">
            <div>ЗВОНИТЕ</div>
            <div className="fs-13">+7(705)-246-22-98</div>
          </div>
          <Divider />
          <div className="flex justify-between items-center">
            <div >ПРИЕЗЖАЙТЕ</div>
            <div>
              <div className="fs-13">г.Байконур , ул. Янгеля</div>
              <div className="fs-13"> д. 2, кв. 24</div>
              <div className="fs-13">Казахстан, 468320</div>
            </div>
          </div>
          <Divider />
        </Paper>
      </div>
    </div>
  );
}
