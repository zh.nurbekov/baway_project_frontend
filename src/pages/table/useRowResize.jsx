import { useCallback, useEffect, useState } from 'react';

export default function useRowResize(rows, tableElement) {
  const [tableHeight, setTableHeight] = useState('auto');
  const [activeIndex, setActiveIndex] = useState(null);

  const mouseDown = index => {
    setActiveIndex(index);
  };

  const mouseMove = useCallback(
    e => {
      const gridColumns = rows.map((col, i) => {
        if (col.id === activeIndex) {
          console.log(col);

          const height = e.clientY - col.ref.current.offsetTop;

          console.log(height, col.ref.current, col.ref.current.offsetTop,e.clientY);
          //if (height >= tableHeight) {
          return `${height}px`;
          //}
        }
        return `${col.ref.current.offsetHeight}px`;
      });
      gridColumns.unshift('63px')
      console.log(gridColumns,gridColumns.join(' '))
      tableElement.current.style.gridTemplateRows = `${gridColumns.join(' ')}`;
    },
    [activeIndex, rows, tableHeight]
  );

  const removeListeners = useCallback(() => {
    window.removeEventListener('mousemove', mouseMove);
    window.removeEventListener('mouseup', removeListeners);
  }, [mouseMove]);

  const mouseUp = useCallback(() => {
    setActiveIndex(null);
    removeListeners();
  }, [setActiveIndex, removeListeners]);

  useEffect(() => {
    if (activeIndex !== null) {
      window.addEventListener('mousemove', mouseMove);
      window.addEventListener('mouseup', mouseUp);
    }

    return () => {
      removeListeners();
    };
  }, [activeIndex, mouseMove, mouseUp, removeListeners]);

  return { tableHeight, setTableHeight, activeIndex, mouseDown };
}
