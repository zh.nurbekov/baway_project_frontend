import React from 'react';
import Table from './Table';
import TableContent from './TableContent';

export default function TableExample() {
  const tableHeaders = ['Items', 'Order #', 'Amount', 'Status', 'Delivery Driver'];

  return (
    <>
      <Table headers={tableHeaders} minCellWidth={120}  />
    </>
  );
}
