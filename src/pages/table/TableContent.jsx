import React, { useRef } from 'react';
import useRowResize from './useRowResize';

export const body = () => [
  {
    ref: useRef(),
    id: 1,
    columns: [
      { name: 'La1rge Detroit Style Pizza' },
      { name: 'Lar2ge Detroit Style Pizza' },
      { name: 'Lar3ge Detroit Style Pizza' },
      { name: 'Lar4ge Detroit Style Pizza' },
      { name: 'Lar5ge Detroit Style Pizza' }
    ]
  },
  {
    id: 2,
    ref: useRef(),
    columns: [
      { name: 'La1rge Deetroit Style Pizza' },
      { name: 'Lar2ge Detroeit Style Pizza' },
      { name: 'Lar3ge Detroit Stryle Pizza' },
      { name: 'Lar4ge Detroit Style Piztza' },
      { name: 'Lar5ge Detroit Style Pizvza' }
    ]
  }
];
const TableContent = ({ tableWidth, tableElement }) => {
  const rows = body();

  const { activeIndex, mouseDown } = useRowResize(rows, tableElement);
  return (
    <tbody>
      {rows.map(({ ref, columns,id }, i) => {
        return (
          <tr>
            {columns.map(({ name }, index) => (
              <td ref={ref} rowSpan={'2'} colSpan={1}>
                <div style={{ position: 'relative' }}>
                  <span>{name}</span>
                  {index === 0 && (
                    <div
                      style={{ width: tableWidth }}
                      onMouseDown={() => mouseDown(id)}
                      className={`resize-height-handle ${activeIndex === i ? 'active' : 'idle'}`}
                    />
                  )}
                </div>
              </td>
            ))}
          </tr>
        );
      })}
    </tbody>
  );
};

export default TableContent;
