import React from 'react';
import { useState, useCallback, useEffect, useRef } from 'react';
import useColumnResize from './useColumnResize';
import TableContent from "./TableContent";

const createHeaders = headers => {
  return headers.map(item => ({
    text: item,
    ref: useRef()
  }));
};

/*
 * Read the blog post here:
 * https://letsbuildui.dev/articles/resizable-tables-with-react-and-css-grid
 */
const Table = ({ headers, minCellWidth }) => {
  const [tableHeight, setTableHeight] = useState('auto');
  const [tableWidth, setTableWidth] = useState('auto');
  const tableElement = useRef(null);
  const columns = createHeaders(headers);
  const { activeIndex, mouseDown } = useColumnResize(minCellWidth, columns, tableElement);

  useEffect(() => {
    setTableHeight(tableElement.current.offsetHeight);
    setTableWidth(tableElement.current.offsetWidth);
    console.log(tableElement.current.offsetWidth);
  }, []);

  // Demo only
  const resetTableCells = () => {
    tableElement.current.style.gridTemplateColumns = '';
  };

  return (
    <div className="container">
      <div className="table-wrapper">
        <table className="resizeable-table" ref={tableElement}>
          <thead>
            <tr>
              {columns.map(({ ref, text }, i) => (
                <th ref={ref} key={text}>
                  <span>{text}</span>
                  <div
                    style={{ height: tableHeight }}
                    onMouseDown={() => mouseDown(i)}
                    className={`resize-handle ${activeIndex === i ? 'active' : 'idle'}`}
                  />
                </th>
              ))}
            </tr>
          </thead>
          <TableContent tableWidth={tableWidth} tableElement={tableElement}/>
        </table>
      </div>
      <button onClick={resetTableCells}>Reset</button>
    </div>
  );
};

export default Table;
