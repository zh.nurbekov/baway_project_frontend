import React from 'react';
import { Wrapper } from './LoginStyles';
import Input from '../../_ui/form/Input';
import Form from '../../_ui/form/Form';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';
import { Button } from 'antd';
import { login } from './LoginDucks';

export const loginValidate = yup.object().shape({
  username: yup.string().required('Обязательное поле для заполнения'),
  password: yup.string().required('Обязательное поле для заполнения')
});

function Login() {
  const dispatch = useDispatch();
  return (
    <Wrapper>
      <div className="col-4 flex items-center mx-auto vertical-center">
        <div className="mx-auto">
          <h3 className="">{'Авторизация'}</h3>
          <Form onSubmit={values => dispatch(login(values))} validate={loginValidate}>
            <Input name="username" placeholder={'Логин'} />
            <Input name="password" type="password" placeholder={'Пароль'} />
            <Button
              htmlType="submit"
              type={'primary'}
              size={'large'}
              style={{
                marginTop: '15px',
                width: '100%'
              }}
            >
              {'ВОЙТИ'}
            </Button>
          </Form>
        </div>
      </div>
    </Wrapper>
  );
}

export default Login;
