import { createReducer } from '@reduxjs/toolkit';
import { LoginApi } from './login.service';
import { hideProgress, showProgress } from '../../components/progressBar/ProgressBarDucks';
import { getRole } from '../usermanage/roles/RolesDucks';
import { UsersApi } from '../usermanage/users/UsersService';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/store';
import { localSecureStorage } from '../../_app/App';

/**
 * Constants
 */

export const loginModule = 'login';
export const LOGIN = `${loginModule}/LOGIN`;
export const PERMISSIONS = `${loginModule}/PERMISSIONS`;
export const LOGOUT = `${loginModule}/LOGOUT`;

/**
 * Reducer
 */

const initialState = {
  user: null,
  permissions: []
};

export default createReducer(initialState, {
  [LOGIN]: (state, action) => ({
    user: action.user
  }),
  [PERMISSIONS]: (state, action) => ({
    permissions: action.permissions
  }),
  [LOGOUT]: state => {
    state.user = null;
  }
});

/**
 * Actions
 */

export const login = values => async dispatch => {
  const { data } = await LoginApi.login(values);
  dispatch(getRole(data.role));
  localSecureStorage.setItem('user', data);
  history.push(paths.home);
};

export const logout = () => async dispatch => {
  dispatch({type: LOGOUT});
 // const {data} = await LoginApi.logoOut();
  localSecureStorage.removeItem('accessToken');
  localSecureStorage.removeItem('refreshToken');
  localSecureStorage.removeItem('expiredAt');
  localSecureStorage.removeItem('user');
  history.push(paths.login);
};
