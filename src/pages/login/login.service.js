import { Api } from '../../_helpers/service';

export const LoginApi = {
  // login: (login, password) => {
  //   let data = new FormData();
  //   data.append('username', login);
  //   data.append('password', password);
  //   data.append('fingerprint', '***');
  //   return Api.post('/login', data);
  // },
  login: values => Api.post('api/login', values),
  logoOut: values => Api.post('/login')
};
