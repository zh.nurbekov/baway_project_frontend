import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height:  100vh;
  align-self: center;
  display: flex;
  justify-content: center;
  align-items: center;

  .vertical-center {
    height: 100%;
  }

  h5 {
    color: red;
  }

  div {
    width: 380px;
    text-align: center;
  }
`;
