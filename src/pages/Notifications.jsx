import React, { useEffect } from 'react';
import Modal from '../_ui/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { messageModule, notificationsIsRead } from './message/MessageDucks';
import { localSecureStorage } from '../_app/App';
import useSimpleModal from '../components/_hooks/useSimpleModal';
import { Button } from 'antd';

export default function Notifications() {
  const { notifications } = useSelector(state => state[messageModule]);
  const dispatch = useDispatch();
  let user = localSecureStorage.getItem('user');
  const modal = useSimpleModal();

  const onClose = () => {
    dispatch(notificationsIsRead(user.id));
    modal.close();
  };

  useEffect(() => {
    console.log(notifications);
    notifications.length && modal.open({});
  }, [notifications]);

  return (
    <>
      {modal.isOpen && (
        <Modal width={'50%'} title={'Уведомление об изменений статуса'} onCancel={() => onClose()}>
          <div>
            {notifications.map(item => (
              <div style={{ color: 'green' , marginBottom:10}}>{item.text}</div>
            ))}
          </div>
          <div className="mt2 right-align">
            <Button type={'primary'} children={'Закрыть'} onClick={() => onClose()} />
          </div>
        </Modal>
      )}
    </>
  );
}
