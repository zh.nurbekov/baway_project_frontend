import React from 'react';

export default function Home() {
  return (
    <div className="p4" style={{ background: 'white', paddingBottom: 50 }}>
      <div className="col-12 flex  py2">
        <div className="col-8" style={{ color: 'black' }}>
          <div style={{ textAlign: 'center' }}>
            Утилизирующая компания «Урал» оказывает полный цикл услуг по утилизации отходов
            производств и потребления, связанных со сбором, транспортировкой и последующим
            обезвреживанием с предъявлением всех необходимых документов и актов, а также фото и
            видео отчетов по запросу Заказчика.
          </div>
          <div style={{ textAlign: 'center' }} className="my2">
            Политика нашей компании направлена на улучшение экологической обстановки региона по
            средствам расширения спектра оказываемых услуг и внедрения новых технологий по
            переработке и утилизации отходов производств и потребления.
          </div>
          <div style={{ textAlign: 'center' }}>
            Цель компании – в максимальной пользе обществу по средствам формирования культуры и
            активной жизненной позиции направленной на улучшение экологической обстановки по
            средствам мотивации и заинтересованности Сторон.
          </div>
        </div>
        <div className="col-4">
          <img width={300} src="/green-planet.jpeg" alt="" />
        </div>
      </div>
      <div className="fs-12 flex items-center justify-around mt4 pb2 px4">
        <div>
          <img width={100} src="/main_one.jpg" alt="" />
          <div>
            ПАСПОРТИЗАЦИЯ И <br />
            ЮРИДИЧЕСКОЕ <br /> СОПРОВОЖДЕНИЕ
          </div>
        </div>
        <div>
          <img width={100} src="/main_two.jpeg" alt="" />
          <div>
            СБОР И ВЫВОЗ ОТХОДОВ <br />
            ПРОИЗВОДСТВА, <br />
            ТРАНСПАРТИРОВКА
          </div>
        </div>
        <div>
          <img width={100} src="/main_tree.jpeg" alt="" />
          <div>
            УТЕЛИЗАЦИЯ ,<br />
            ПЕРЕРАБОТКА ,<br />
            ОБЕЗВРЕЖИВАНИЕ
          </div>
        </div>
      </div>
    </div>
  );
}
