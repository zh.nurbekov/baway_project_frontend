import Modal from '../../_ui/Modal';
import React, { useEffect, useState } from 'react';
import { Button, Card, Descriptions, Table } from 'antd';
import { catalogDescTableColumns } from './CatalogDescTableColumns';
import { Api } from '../../_helpers/service';

export default function StatementTransportServiceModal({ close, statements }) {
  const [data, setData] = useState([]);

  const loadData = () => {
    let statement = '';
    for (let i in statements) {
      statement += `statmentId=${statements[i]}`;
      if (parseInt(i) !== statements.length - 1) {
        statement += `&`;
      }
    }
    Api.get(`api/transport/service/statments?${statement}`)
      .then(({ data }) => setData(data))
      .catch(e => console.log(e));
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <>
      <Modal width={'90%'} title={'Транспортных услуги'} onCancel={close}>
        {data.map(item => {
          return (
            <Card title="Расчет транспорта" bordered style={{ width: 350 }}>
              <p>{`Класс опасности : ${item.classDanger}`}</p>
              <p>{`Кол-во транспорта : ${item.countTransport} ед`}</p>
              <p>{`Общий объем : ${item.totalVolume} m3`}</p>
              <p>{`Транспорт : ${item.transport}`}</p>
              {item.statments.length !== 0 && (
                <div style={{padding:6, background:'#a2d6d7', borderRadius:8}}>
                  {<h6>{`Организаций`}</h6>}
                  {item.statments.map(item => (
                    <p>{item}</p>
                  ))}
                </div>
              )}
            </Card>
          );
        })}
      </Modal>
    </>
  );
}
