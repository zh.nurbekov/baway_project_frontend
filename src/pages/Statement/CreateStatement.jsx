import React, { useEffect, useState } from 'react';
import Form from '../../_ui/form/Form';
import Input from '../../_ui/form/Input';
import { Button, Checkbox, Table } from 'antd';
import { Api } from '../../_helpers/service';
import { Input as AntInput } from 'antd';
import { Paper } from '@material-ui/core';
import { catalogTableColumns, StatementValidate } from './CatalogTableColumns';
import statusMessage from '../../utils/statusMessage';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/store';
import { NotificationManager } from 'react-notifications';
import { localSecureStorage } from '../../_app/App';
import Select from '../../_ui/Select';
const { TextArea } = AntInput;

export default function CreateStatement({ close }) {
  const [catalog, setCatalog] = useState([]);
  const [info, setInfo] = useState('');
  const [transportServices, setTransportServices] = useState(false);
  const [isAllSelected, setIsAllSelected] = useState(false);
  const [name, setName] = useState('');
  const [classDanger, setClassDanger] = useState();
  const [classDangerList, setClassDangerList] = useState([]);
  let user = localSecureStorage.getItem('user');

  const onChange = params => {
    setCatalog([...catalog.map(item => (item.id === params.id ? params : item))]);
  };

  useEffect(() => {
    Api.get('api/catalog/items', {
      params: { classDanger, name, limit: 1000, offset: 0 }
    })
      .then(({ data }) => setCatalog(data))
      .catch(e => console.error(e));
  }, [classDanger, name]);

  useEffect(() => {
    Api.get('api/get/catalog/class')
      .then(({ data }) => setClassDangerList(data))
      .catch(e => console.error(e));
  }, []);

  const onSubmit = values => {
    const catalogList = isAllSelected
      ? [...catalog]
      : catalog.filter(({ isSelected }) => isSelected);

    const list = catalogList.map(item => ({
      ...item,
      catalogItemId: item.id
    }));

    const props = {
      legalAddress: values.legalAddress,
      mailingAddress: values.mailingAddress,
      iin: values.iin,
      kpp: values.kpp,
      correspondentScore: values.correspondentScore,
      estimatedScore: values.estimatedScore,
      bik: values.bik
    };
    const requestData = {
      ...values,
      props,
      information: info,
      transportServices,
      catalog: list,
      userId: user.id
    };

    if (list.length) {
      Api.post('api/create/statement', requestData)
        .then(({ data }) => {
          statusMessage(data, () => history.push(paths.home));
        })
        .catch(e => console.error(e));
    } else {
      NotificationManager.warning('Выберите данные по каталогу');
    }
  };

  return (
    <>
      <Form onSubmit={onSubmit} validate={StatementValidate}>
        <div className="right-align flex justify-between">
          <h3 children={'Подача заявки'} className="mb2 bold" />
          <Button type={'primary'} htmlType="submit" children={'Отправить'} />
        </div>
        <div className="pt1 pb4">
          <div className="py1">
            <Paper style={{ marginBottom: 16, paddingBottom: 16 }}>
              <div className="flex  p2">
                <div className="md-col-6 pr1">
                  <Input label="Наименование организации *" name={'organization'} />
                  <Input
                    label="ФИО представителя организации*"
                    name={'organizationRepresentative'}
                  />
                  <div className="flex items-center">
                    <Input
                      label="Контактные данные *"
                      name={'phone'}
                      style={{ paddingRight: 10 }}
                    />
                    <Input
                      label="Почта *"
                      name={'email'}
                      type={'email'}
                      style={{ paddingLeft: 10 }}
                    />
                  </div>
                </div>

                <div className="md-col-6 pl1">
                  <div children={'Дополнение'} className="mb1" />
                  <TextArea
                    value={info}
                    onChange={({ target: { value } }) => setInfo(value)}
                    style={{ height: 115 }}
                    rows={4}
                  />
                </div>
              </div>
              <Checkbox
                checked={transportServices}
                onChange={({ target: { checked } }) => setTransportServices(checked)}
                children="Транспортные услуги"
                style={{ marginLeft: 20 }}
              />
            </Paper>
            <Paper style={{ padding: 20, marginBottom: 20 }}>
              <h6> Реквизиты</h6>
              <div className="flex ">
                <Input label="Юр. адрес *" name={'legalAddress'} style={{ marginRight: 10 }} />
                <Input
                  label="Почтовый адрес *"
                  name={'mailingAddress'}
                  style={{ marginRight: 10 }}
                />
                <Input label="ИИН *" name={'iin'} style={{ marginRight: 10 }} />
                <Input label="КПП *" name={'kpp'} style={{ marginRight: 10 }} />
              </div>

              <div className="flex ">
                <Input
                  label="Расчетный/счёт: *"
                  name={'correspondentScore'}
                  style={{ marginRight: 10 }}
                />
                <Input
                  label="Корреспондентский/счёт *"
                  name={'estimatedScore'}
                  style={{ marginRight: 10 }}
                />
                <Input label="БИК *" name={'bik'} />
              </div>
            </Paper>
            <Paper style={{ borderRadius: 8, paddingTop: 10 }}>
              <div className="flex items-center mb2 mt3 ml2">
                <div className=" mr2">
                  <div className="mb1 mr2">Поиск по имени</div>
                  <AntInput
                    style={{ width: 250, height: 38 }}
                    value={name}
                    onChange={({ target: { value } }) => setName(value)}
                  />
                </div>

                <Select
                  label={'Категории'}
                  width={250}
                  withoutForm
                  value={classDanger}
                  onChange={value => setClassDanger(value)}
                  options={classDangerList.map(({ name, id }) => ({ name, value: id }))}
                />
              </div>
              <div className="p2">
                <Table
                  columns={catalogTableColumns(onChange)}
                  dataSource={catalog}
                  bordered
                  rowSelection={{
                    onSelect: row => onChange({ ...row, isSelected: !row.isSelected }),
                    onSelectAll: e => setIsAllSelected(e)
                  }}
                  pagination={false}
                />
              </div>
            </Paper>
          </div>
        </div>
      </Form>
    </>
  );
}
