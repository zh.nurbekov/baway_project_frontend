import { Api } from '../../_helpers/service';

export const StatementsApi = {
  loadStatement: (params) => Api.get('api/statements',{params})
};
