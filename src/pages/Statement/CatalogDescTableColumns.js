import React from 'react';

export const catalogDescTableColumns = () => {
  return [
    {
      title: 'Продукт',
      dataIndex: 'name'
    },
    {
      title: 'Оплата за транспорт',
      dataIndex: 'paymentTransport',
      width: 200
    },
    {
      title: 'Кол-во',
      dataIndex: 'cout',
      width: 200
    },
    {
      title: 'Цена',
      dataIndex: 'price',
      width: 200
    },
    {
      title: 'Итого',
      dataIndex: 'total',
      width: 200
    }
  ];
};
