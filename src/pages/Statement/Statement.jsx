import React, { useState } from 'react';
import { Paper } from '@material-ui/core';
import Table from '../../_ui/Table3/Table';
import { loadStatements, statementModule } from './StatementDucks';
import { useDispatch, useSelector } from 'react-redux';
import { statementColumns } from './StatementColumns';
import StatementDescriptionModal from './StatementDescriptionModal';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import { Button } from 'antd';
import StatementTransportServiceModal from './StatementTransportServiceModal';

const statementFilterName = '_statement';
export default function Statement() {
  const { statements } = useSelector(state => state[statementModule]);
  const dispatch = useDispatch();
  const modal = useSimpleModal();
  const transportModal = useSimpleModal();
  const [statement, setStatement] = useState([]);
  return (
    <>
      <div className="flex items-center justify-between">
        <h3 children={'Заявки'} className="mb2 bold" />
        <Button
          onClick={() => transportModal.open(statement)}
          type={'primary'}
          children={'Рассчитать транспортные услуги'}
        />
      </div>
      <Paper style={{ padding: 10 }}>
        <Table
          onClickRow={({ original }) => modal.open(original)}
          queryFilterName={statementFilterName}
          columns={statementColumns(statement, setStatement)}
          data={statements}
          isSearch={true}
          total={statements.length}
          loadData={filter => dispatch(loadStatements(filter))}
        />
      </Paper>
      {modal.isOpen && <StatementDescriptionModal {...modal} />}
      {transportModal.isOpen && (
        <StatementTransportServiceModal {...transportModal} statements={statement} />
      )}
    </>
  );
}
