import Modal from '../../_ui/Modal';
import React, { useState } from 'react';
import { Button, Card, Descriptions, Table } from 'antd';
import { catalogDescTableColumns } from './CatalogDescTableColumns';
import { Api } from '../../_helpers/service';

export default function StatementDescriptionModal({ close, data }) {
  const [document, setDocument] = useState();
  const [protocol, setProtocol] = useState();
  const [act, setAct] = useState();
  const [payment, setPayment] = useState();
  const [passport, setPassport] = useState();

  const loadDocument = () => {
    Api.get(`api/genered/report/contract/${data.id}`)
      .then(({ data }) => {
        console.log(data);
        setDocument(data.message);
      })
      .catch(e => console.log(e));
  };

  const loadProtocol = () => {
    Api.get(`api/genered/report/protocol/${data.id}`)
      .then(({ data }) => setProtocol(data.message))
      .catch(e => console.log(e));
  };

  const loadAct = () => {
    Api.get(`api/genered/report/act/${data.id}`)
      .then(({ data }) => setAct(data.message))
      .catch(e => console.log(e));
  };

  const loadPassport = () => {
    Api.get(`api/genered/report/passport/calculation/${data.id}`)
      .then(({ data }) => setPassport(data.message))
      .catch(e => console.log(e));
  };

  const loadPayment = () => {
    Api.get(`api/genered/report/invoice/payment/${data.id}`)
      .then(({ data }) => setPayment(data.message))
      .catch(e => console.log(e));
  };

  return (
    <>
      <Modal width={'90%'} title={'Информация о заявке'} onCancel={close}>
        <div className="pb2">
          <Descriptions title={data.organization} bordered layout={'vertical'} className="mb1">
            <Descriptions.Item
              label="Реквезиты"
              children={
                <div>
                  <div>{`Юр. адрес : ${data.props.legalAddress}`}</div>
                  <div>{`Почтовый адрес : ${data.props.mailingAddress}`}</div>
                  <div>{`ИИН : ${data.props.iin}`}</div>
                  <div>{`КПП : ${data.props.kpp}`}</div>
                  <div>{`Расчетный/счёт : ${data.props.correspondentScore}`}</div>
                  <div>{`Корреспондентский/счёт : ${data.props.estimatedScore}`}</div>
                  <div>{`БИК : ${data.props.bik}`}</div>
                </div>
              }
            />
            <Descriptions.Item label="Контактные данные" children={data.phone} />
            <Descriptions.Item label="Почта" children={data.email} />
            <Descriptions.Item
              label="Транспортные услуги"
              children={data.transportServices ? 'ДА' : 'НЕТ'}
            />
            <Descriptions.Item
              label="Дополнительное информация"
              children={data.information}
              span={3}
            />
          </Descriptions>
          <Table
            bordered
            columns={catalogDescTableColumns()}
            dataSource={data.catalog}
            pagination={false}
          />
          <Descriptions bordered layout={'vertical'} className="mb1 mt2" column={5}>
            <Descriptions.Item label="Договор">
              {document ? (
                <a href={document}>
                  <Button children={'Скачать '} />
                </a>
              ) : (
                <Button children={'Сгенерировать '} onClick={loadDocument} />
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Протокол согласования договорной цены">
              {protocol ? (
                <a href={protocol}>
                  <Button children={'Скачать '} />
                </a>
              ) : (
                <Button children={'Сгенерировать '} onClick={loadProtocol} />
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Акт приема-передачи мусора и отходов">
              {act ? (
                <a href={act}>
                  <Button children={'Скачать '} />
                </a>
              ) : (
                <Button children={'Сгенерировать '} onClick={loadAct} />
              )}
            </Descriptions.Item>

            <Descriptions.Item label="Расчет Паспорта">
              {passport ? (
                <a href={passport}>
                  <Button children={'Скачать '} />
                </a>
              ) : (
                <Button children={'Сгенерировать '} onClick={loadPassport} />
              )}
            </Descriptions.Item>

            <Descriptions.Item label="Счет на оплату">
              {payment ? (
                <a href={payment}>
                  <Button children={'Скачать '} />
                </a>
              ) : (
                <Button children={'Сгенерировать '} onClick={loadPayment} />
              )}
            </Descriptions.Item>
          </Descriptions>
        </div>
        {/*{data.dataTransportServices &&*/}
        {/*  data.dataTransportServices.map(item => {*/}
        {/*    return (*/}
        {/*      <Card title="Расчет транспорта" bordered style={{ width: 350 }}>*/}
        {/*        <p>{`Класс опасности : ${item.classDanger}`}</p>*/}
        {/*        <p>{`Кол-во транспорта : ${item.countTransport}`}</p>*/}
        {/*        <p>{`Общий объем : ${item.totalVolume}`}</p>*/}
        {/*        <p>{`Транспорт : ${item.transport}`}</p>*/}
        {/*      </Card>*/}
        {/*    );*/}
        {/*  })}*/}
      </Modal>
    </>
  );
}
