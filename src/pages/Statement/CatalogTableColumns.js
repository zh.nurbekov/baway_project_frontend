import { Input as AntInput } from 'antd';
import React from 'react';
import * as yup from 'yup';

//для формы валидация полей
export const StatementValidate = yup.object().shape({
  organization: yup.string().required('Обязательное поле для заполнения'),
  email: yup.string().required('Обязательное поле для заполнения'),
  organizationRepresentative: yup.string().required('Обязательное поле для заполнения'),
  phone: yup.string().required('Обязательное поле для заполнения')
});

export const catalogTableColumns = onChange => {
  return [
    {
      title: 'Каталог',
      dataIndex: 'name'
    },

    {
      title: 'Кол-во',
      dataIndex: '2',
      width: 200,
      render: (text, row) => {
        return (
          <AntInput
            size="large"
            type="number"
            onChange={({ target: { value } }) => onChange({ ...row, cout: value })}
          />
        );
      }
    },
    {
      title: 'Цена',
      dataIndex: '3',
      width: 200,
      render: (text, row) => {
        return (
          <AntInput
            disabled
            value={row.price}
            size="large"
            type="number"
            onChange={({ target: { value } }) => onChange({ ...row, price: value })}
          />
        );
      }
    },
    {
      title: 'Итого',
      dataIndex: '4',
      width: 200,
      render: (text, row) => {
        return (
          <AntInput
            disabled
            value={row.price * row.cout}
            size="large"
            type="number"
            onChange={({ target: { value } }) => onChange({ ...row, total: value })}
          />
        );
      }
    }
  ];
};
