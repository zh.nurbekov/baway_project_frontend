import { createReducer } from '@reduxjs/toolkit';
import {StatementsApi} from "./StatementsService";

/**
 * Constants
 */

export const statementModule = 'statement';
export const STATEMENT = `${statementModule}/STATEMENT`;
export const CREATE = `${statementModule}/CREATE`;
export const UPDATE = `${statementModule}/UPDATE`;
export const DELETE = `${statementModule}/DELETE`;
export const LOADING = `${statementModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  statements: [],
  loading: false
};

export default createReducer(initialState, {
  [STATEMENT]: (state, { payload }) => {
    state.statements = payload;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.users.content = [payload, ...state.users.content];
    state.users.count = state.users.count + 1;
  },
  [UPDATE]: (state, { payload }) => {
    state.users.content = [
      ...state.users.content.map(item => (item.id === payload.id ? payload : item))
    ];
  },
  [DELETE]: (state, { payload }) => {
    state.users.content = [...state.users.content.filter(({ id }) => id !== payload)];
    state.users.count = state.users.count !== 0 ? state.users.count - 1 : 0;
  },

  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadStatements = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await StatementsApi.loadStatement(filter);
    dispatch({ type: STATEMENT, payload: data});
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};





