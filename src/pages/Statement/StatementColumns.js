import React from 'react';
import { localSecureStorage } from '../../_app/App';
import { EMPOLYEE } from '../../_helpers/Constants';
import { Checkbox } from '@material-ui/core';
import { Button } from 'antd';

export const statementColumns = (statement, setStatement) => {
  let user = localSecureStorage.getItem('user');

  const columns = [
    {
      Header: '',
      accessor: 'rowNum',
      width: 80,
      filterable: false,
      Cell: ({ original }) => {
        return (
          <Checkbox
            color={'primary'}
            onClick={e => e.stopPropagation()}
            checked={statement.includes(original.id)}
            onChange={({ target: { checked } }) => {
              checked
                ? setStatement([...statement, original.id])
                : setStatement([...statement.filter(id => id !== original.id)]);
            }}
          />
        );
      }
    },
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      filterable: false
    },
    {
      Header: 'Организация',
      accessor: 'organization'
    },
    { Header: 'Контактные данные', accessor: 'phone' },
    { Header: 'Почта', accessor: 'email' },
    {
      Header: '',
      accessor: 'email',
      Cell: row => (
        <a href={'https://cent.app/'} target={'_blank'} onClick={e => e.stopPropagation()}>
          Оплатить
        </a>
      )
    }
  ];

  const transport = [
    {
      Header: 'Класс опасности',
      accessor: 'class_danger'
    },
    {
      Header: 'Вес',
      accessor: 'volume'
    },
    {
      Header: 'Объем',
      accessor: 'weight_ton'
    }
  ];
  // hasAccess(PERMISSIONS.USER_EDIT) &&
  //columns.push(actions);
  //columns.push(transport);

  return user.userType === EMPOLYEE ? [...columns, ...transport] : columns;
};
