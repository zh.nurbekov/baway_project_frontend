import { Button, Tooltip } from 'antd';
import { confirm } from '../../../utils/Utils';
import React from 'react';
import * as yup from 'yup';
import Access from "../../../components/access/Access";
import {PERMISSIONS} from "../../../_helpers/Permissions";
import hasAccess from "../../../components/access/hasAccess";

//для формы валидация полей
export const UsersValidate = yup.object().shape({
  firstname: yup.string().required('Обязательное поле для заполнения'),
  middlename: yup.string().required('Обязательное поле для заполнения'),
  username: yup.string().required('Обязательное поле для заполнения'),
  password: yup.string().required('Обязательное поле для заполнения'),
  birthdate: yup.string().required('Обязательное поле для заполнения'),
  email: yup.string().required('Обязательное поле для заполнения'),
  role: yup.number().required('Обязательное поле для заполнения'),
  gender: yup.string().required('Обязательное поле для заполнения'),
  phone: yup.string().required('Обязательное поле для заполнения'),
  userType: yup.number().required('Обязательное поле для заполнения')
});

export const userColumns = (updateUser, deleteUser, userType, modalOnOpen) => {
  const getType = typeId => {
    const type = userType.find(({ id }) => id === typeId);
    return type ? type.name : '';
  };

  const onDelete = id => {
    confirm('Вы действительно хотите удалить пользователя?', () => {
      deleteUser(id);
    });
  };

  const columns = [
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      filterable: false
    },
    {
      Header: 'Имя',
      accessor: 'firstname'
    },
    { Header: 'Фамилия', accessor: 'middlename', filterable: false },
    { Header: 'Роль', accessor: 'roleName', filterable: false },
    {
      Header: 'Тип пользователя',
      accessor: 'userType',
      Cell: ({ original }) => {
        return original.userType ? <div>{getType(original.userType)}</div> : null;
      }
    }
  ];

  const actions = {
    Header: 'Действие',
    accessor: 'is_active',
    width: 200,
    Cell: row => (
      <div className="center">
        <Tooltip title="Редактировать">
          <Button
            className="ml1"
            type={'link'}
            size={'small'}
            color="primary"
            icon={'edit'}
            onClick={() => modalOnOpen(row.original)}
          />
        </Tooltip>
        <Access permissions={PERMISSIONS.USER_DELETE}>
        <Tooltip title="Удалить пользователя">
          <Button
            className="mr1"
            type={'link'}
            size={'small'}
            color="primary"
            icon={'delete'}
            onClick={() => onDelete(row.original.id)}
          />
        </Tooltip>
        </Access>
      </div>
    )
  };

   hasAccess(PERMISSIONS.USER_EDIT) &&
  columns.push(actions);

  return columns;
};
