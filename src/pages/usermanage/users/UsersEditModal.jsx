import React from 'react';
import { Button } from 'antd';
import Form from '../../../_ui/form/Form';
import { createUser, updateUser, usersModule } from './UsersDucks';
import { UsersValidate } from './UserColumns';
import { useDispatch, useSelector } from 'react-redux';
import Select from '../../../_ui/Select';
import Input from '../../../_ui/form/Input';
import Modal from '../../../_ui/Modal';
import { Male } from '../../../_helpers/Constants';
import getOptions from '../../../utils/getOptions';

function UsersEditModal({ dataIsEmpty, data, close: modalOnClose }) {
  const { roles, userType } = useSelector(state => state[usersModule]);
  const dispatch = useDispatch();

  const onSubmit = values => {
    return dataIsEmpty
      ? dispatch(createUser(values, modalOnClose))
      : dispatch(updateUser(values, modalOnClose));
  };

  return (
    <div>
      <Modal
        width={'70%'}
        title={dataIsEmpty ? 'Добавление пользователя' : 'Редактирование пользователя'}
        onCancel={() => modalOnClose()}
      >
        <Form validate={UsersValidate} initialValues={data} onSubmit={onSubmit}>
          <div className="flex">
            <div className="md-col-6 px1">
              <Input label="Фамилия" name="middlename" />
              <Input label="Имя" name="firstname" />
              <Input label="Отчество" name="lastname" />
              <Input label="Дата рождения" name="birthdate" />
              <Input label="Логин" name={'username'} />
              <Input label="Пароль" name={'password'} type={'password'}/>
            </div>
            <div className="md-col-6 px1">
              <Input label="Почта" name={'email'} type={'email'} />
              <Input label="Контактные данные" name={'phone'} type={'number'}/>
              <Select
                fullWidth
                label={'Тип пользователя'}
                variant={'outlined'}
                options={getOptions(userType)}
                name={'userType'}
              />
              <Select
                fullWidth
                label={'Роль'}
                variant={'outlined'}
                options={getOptions(roles)}
                name={'role'}
              />
              <Select fullWidth label={'Пол'} variant={'outlined'} options={Male} name={'gender'} />
              <Input
                multiline
                rows={3}
                fullWidth
                label="Дополнительная информация"
                name={'information'}
              />
            </div>
          </div>

          <div className="mt2 right-align">
            <Button
              type={'primary'}
              htmlType="submit"
              children={dataIsEmpty ? 'Добавить' : 'Сохранить'}
              icon={dataIsEmpty ? 'plus' : 'save'}
            />
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default UsersEditModal;
