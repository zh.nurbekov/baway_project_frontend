import { Api } from '../../../_helpers/service';
import axios from 'axios';

export const UsersApi = {
  deleteUser: id => Api.delete(`api/delete/user/${id}`),
  loadCategory: () => Api.get('api/categories'),
  loadUserType: () => Api.get('api/usertypes'),
  getUser: id => Api.get(`api/user/${id}`),
  updateUser: user => Api.put(`api/update/user`, user),
  createUser: user => Api.post('api/registr', user),
  getRoles: () => Api.get('api/roles'),
  getUsers: params => {
    let data = new FormData();
    const json = JSON.stringify(params);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('filter', blob);
    return axios.post('api/search/users', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
