import { createReducer } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { hideProgress, showProgress } from '../../../components/progressBar/ProgressBarDucks';
import { UsersApi } from './UsersService';
import statusMessage from '../../../utils/statusMessage';

/**
 * Constants
 */

export const usersModule = 'users';
export const USERS = `${usersModule}/USERS`;
export const ROLES = `${usersModule}/ROLES`;
export const CREATE = `${usersModule}/CREATE`;
export const UPDATE = `${usersModule}/UPDATE`;
export const DELETE = `${usersModule}/DELETE`;
export const USER_TYPE = `${usersModule}/USER_TYPE`;
export const LOADING = `${usersModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  users: { content: [], count: 0 },
  roles: [],
  userType: [],
  loading: false
};

export default createReducer(initialState, {
  [USERS]: (state, { payload }) => {
    state.users = payload;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.users.content = [payload, ...state.users.content];
    state.users.count = state.users.count + 1;
  },
  [UPDATE]: (state, { payload }) => {
    state.users.content = [
      ...state.users.content.map(item => (item.id === payload.id ? payload : item))
    ];
  },
  [DELETE]: (state, { payload }) => {
    state.users.content = [...state.users.content.filter(({ id }) => id !== payload)];
    state.users.count = state.users.count !== 0 ? state.users.count - 1 : 0;
  },
  [ROLES]: (state, { payload }) => {
    state.roles = payload;
  },
  [USER_TYPE]: (state, { payload }) => {
    state.userType = payload;
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadUsers = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await UsersApi.getUsers(filter);
    let { data: roles } = await UsersApi.getRoles();
    const content = data.data.map(item => {
      const role = roles.find(role => role.id === item.role);
      return { ...item, roleName: role ? role.name || '' : '' };
    });
    dispatch({ type: ROLES, payload: roles });
    dispatch({ type: USERS, payload: { content, count: data.count || 0 } });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createUser = (user, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    let { roles } = getState()[usersModule];
    const { data } = await UsersApi.createUser(JSON.stringify(user));

    const role = roles.find(role => role.id === user.role);
    const content = { ...user, roleName: role ? role.name || '' : '' };
    const success = ()=>{
      dispatch({ type: CREATE, payload: content });
      modalOnClose && modalOnClose();
    }
    statusMessage(data,success);

    dispatch(hideProgress());
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const updateUser = (user, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    let { roles } = getState()[usersModule];
    let { data } = await UsersApi.updateUser(user);
    const role = roles.find(role => role.id === data.role);
    const content = { ...data, roleName: role ? role.name || '' : '' };
    dispatch({ type: UPDATE, payload: content });
    NotificationManager.success('Данные успешно сохранены');
    modalOnClose && modalOnClose();
    dispatch(hideProgress());
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const deleteUser = (id, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    await UsersApi.deleteUser(id);
    dispatch({ type: DELETE, payload: id });
    NotificationManager.success('Данные успешно удалены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const loadUserType = () => async dispatch => {
  try {
    let { data } = await UsersApi.loadUserType();
    dispatch({ type: USER_TYPE, payload: data });
  } catch (e) {
    console.error(e);
  }
};
