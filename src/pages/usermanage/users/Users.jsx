import React, {useEffect} from 'react';
import {deleteUser, loadUsers, loadUserType, updateUser, usersModule} from './UsersDucks';
import { userColumns } from './UserColumns';
import UsersEditModal from './UsersEditModal';
import { useDispatch, useSelector } from 'react-redux';
import useSimpleModal from '../../../components/_hooks/useSimpleModal';
import { Button } from 'antd';
import Table from '../../../_ui/Table3/Table';

const userFilterName = '_user';
function Users() {
  const { users,userType, loading } = useSelector(state => state[usersModule]);
  const dispatch = useDispatch();
  const modal = useSimpleModal();
  const update = user => dispatch(updateUser(user));
  const deleteUsr = id => dispatch(deleteUser(id));

    useEffect(() => {
        dispatch(loadUserType());
    }, []);


    return (
    <>
      <div className="right-align mb1">
        <Button
          type={'primary'}
          icon={'plus'}
          children={'Добавить'}
          onClick={() => modal.open({})}
        />
      </div>
      <Table
        queryFilterName={userFilterName}
        columns={userColumns(update, deleteUsr,userType, modal.open)}
        data={users.content}
        isSearch={true}
        loading={loading}
        total={users.count}
        loadData={filter => dispatch(loadUsers(filter))}
      />
      {modal.isOpen && <UsersEditModal {...modal} />}
    </>
  );
}

export default Users;
