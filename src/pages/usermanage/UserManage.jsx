import React from 'react';
import { Tabs } from 'antd';
import Users from '../usermanage/users/Users';
import Roles from '../usermanage/roles/Roles';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/store';
import { useLocation } from 'react-router-dom';
import Access from '../../components/access/Access';
import { PERMISSIONS } from '../../_helpers/Permissions';
import hasAccess from '../../components/access/hasAccess';

const TabPane = Tabs.TabPane;

function UserManage() {
  const { pathname } = useLocation();
  return (
    <div>
      <h3 children={'Администрирование'} className="mb2 bold" />
      <Tabs
        type="card"
        style={{ marginBottom: '20px', width: '100%' }}
        activeKey={pathname}
        onChange={tab => history.push(tab)}
      >
        {hasAccess(PERMISSIONS.USERS) && (
          <TabPane tab={'Пользователи'} key={paths.user}>
            <Users />
          </TabPane>
        )}
        {hasAccess(PERMISSIONS.ROLES) && (
          <TabPane tab={'Роли'} key={paths.role}>
            <Roles />
          </TabPane>
        )}
      </Tabs>
    </div>
  );
}


export default UserManage;
