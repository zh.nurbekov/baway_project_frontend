import React from 'react';
import { Tree as AntTree } from 'antd';
import lodash from 'lodash';
import { PERMISSION_NAMES, PERMISSIONS_TREE } from '../../../_helpers/Permissions';
const { TreeNode } = AntTree;

const Tree = ({ onChecked, onClickHandle, permissions }) => {
  const threeNode = () => {
    const three = [];
    lodash.forIn(PERMISSIONS_TREE, (values, key) => {
      return three.push(
        <TreeNode
          checkable
          title={PERMISSION_NAMES[key]}
          key={key}
          dataRef={key}
          selectable
          style={{ padding: 0 }}
        >
          {values.map(item => (
            <TreeNode
              checkable
              selectable
              title={PERMISSION_NAMES[item]}
              key={item}
              dataRef={item}
              style={{ padding: 0 }}
            />
          ))}
        </TreeNode>
      );
    });
    return <TreeNode children={three} key={'all'} title={'Все'} selectable />;
  };

  return (
    <AntTree
      checkable
      checkStrictly
      defaultExpandAll
      onCheck={onChecked}
      checkedKeys={permissions}
      onClick={(e, v) => onClickHandle(v.props.eventKey)}
    >
      {threeNode()}
    </AntTree>
  );
};

export default Tree;
