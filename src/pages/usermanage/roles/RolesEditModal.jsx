import React from 'react';
import { Button } from 'antd';
import Form from '../../../_ui/form/Form';
import { createRoles, updateRoles } from './RolesDucks';
import { RolesValidate } from './RoleUtils';
import useTreePermissions from './useTreePermissions';
import { useDispatch } from 'react-redux';
import Tree from './Tree';
import Input from '../../../_ui/form/Input';
import Modal from '../../../_ui/Modal';

function RolesEditModal({ data, close: modalOnClose, dataIsEmpty }) {
  const dispatch = useDispatch();
  const { onChecked, onClickHandle, permissions } = useTreePermissions(dataIsEmpty ? null : data);

  const onSubmit = values => {
    const formData = { ...values, access_list: permissions };
    dataIsEmpty
      ? dispatch(createRoles(formData, modalOnClose))
      : dispatch(updateRoles(formData, modalOnClose));
  };

  return (
    <div>
      <Modal
        title={dataIsEmpty ? 'Добавление роли' : 'Редактирование роли'}
        onCancel={() => modalOnClose()}
      >
        <Form initialValues={data} validate={RolesValidate} onSubmit={onSubmit}>
          <Input name="name" label={'Наименование*'} />
          <div className="mb1">
            <div children={'Права доступа'} />
            <Tree onChecked={onChecked} permissions={permissions} onClickHandle={onClickHandle} />
          </div>
          <div className="mt2 right-align">
            <Button
              type={'primary'}
              htmlType="submit"
              icon={dataIsEmpty ? 'plus' : 'save'}
              children={dataIsEmpty ? 'Добавить' : 'Сохранить'}
            />
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default RolesEditModal;
