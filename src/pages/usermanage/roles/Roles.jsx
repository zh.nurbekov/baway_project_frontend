import React from 'react';
import { roleColumns } from './RoleUtils';
import RolesEditModal from './RolesEditModal';
import { PERMISSIONS } from '../../../_helpers/Permissions';
import { getRoles, rolesModule, updateRoles } from './RolesDucks';
import { useDispatch, useSelector } from 'react-redux';
import Access from '../../../components/access/Access';
import useSimpleModal from '../../../components/_hooks/useSimpleModal';
import { Button } from 'antd';
import Table from '../../../_ui/Table3/Table';

const roleFilterName = '_role';
function Roles() {
  const { roles, loading } = useSelector(state => state[rolesModule]);
  const dispatch = useDispatch();
  const modal = useSimpleModal();
  const update = role => dispatch(updateRoles(role));

  return (
    <div>
      <div className="right-align mb1">
        <Button type={'primary'} children={'Добавить'} icon={'plus'} onClick={() => modal.open()} />
      </div>
      <Table
        queryFilterName={roleFilterName}
        columns={roleColumns(update, modal.open)}
        data={roles}
        isSearch={true}
        loading={loading}
        total={roles.length}
        loadData={filter => dispatch(getRoles(filter))}
      />
      {modal.isOpen && <RolesEditModal {...modal} />}
    </div>
  );
}

export default Roles;
