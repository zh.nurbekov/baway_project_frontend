import { createReducer } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { RolesApi } from './RolesService';
import { hideProgress, showProgress } from '../../../components/progressBar/ProgressBarDucks';
import { localSecureStorage } from '../../../_app/App';

/**
 * Constants
 */

export const rolesModule = 'roles';
export const ROLES = `${rolesModule}/ROLES`;
export const CREATE = `${rolesModule}/CREATE`;
export const UPDATE = `${rolesModule}/UPDATE`;
export const LOADING = `${rolesModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  roles: [],
  loading: false
};

export default createReducer(initialState, {
  [ROLES]: (state, { payload }) => {
    state.roles = payload;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.roles = [payload, ...state.roles];
  },
  [UPDATE]: (state, { payload }) => {
    state.roles = [...state.roles.map(item => (item.id === payload.id ? payload : item))];
  },
  [LOADING]: (state, action) => {
    state.loading = action.loading;
  }
});

/**
 * Actions
 */

export const getRoles = () => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await RolesApi.getRoles();
    const roles = data.map(item => ({ ...item, access_list: JSON.parse(item.access_list) }));
    dispatch({ type: ROLES, payload: roles });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const getRole = id => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    const user = localSecureStorage.getItem('user');
    let {
      data: { access_list }
    } = await RolesApi.getRole(id);
    localSecureStorage.setItem('user', { ...user, accessList: JSON.parse(access_list) });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createRoles = (role, modalOnClose) => async dispatch => {
  try {
    dispatch(showProgress());
    role.access_list = JSON.stringify(role.access_list);
    let { data } = await RolesApi.createRoles(role);
    dispatch({ type: CREATE, payload: data });
    NotificationManager.success('Данные успешно сохранены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const updateRoles = (role, modalOnClose) => async dispatch => {
  try {
    dispatch(showProgress());
    role.access_list = JSON.stringify(role.access_list);
    let { data } = await RolesApi.updateRole(role);
    console.log(data);
    dispatch({ type: UPDATE, payload: data });
    NotificationManager.success('Данные успешно сохранены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    console.error(e);
    dispatch(hideProgress());
  }
};
