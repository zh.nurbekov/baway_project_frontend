import { PERMISSIONS } from '../../../_helpers/Permissions';
import { Button, Switch, Tooltip } from 'antd';
import React from 'react';
import * as yup from 'yup';
import hasAccess from '../../../components/access/hasAccess';

//для формы валидация полей
export const RolesValidate = yup.object().shape({
  name: yup.string().required('Обязательное поле для заполнения')
});

// для таблицы загаловки и т.д
export const roleColumns = (update, modalOnOpen) => {
  const columns = [
    { Header: '№', accessor: 'rowNum', width: 80 },
    { Header: 'Наименование', accessor: 'name' }
  ];

  const actions = {
    Header: 'Действие',
    accessor: 'is_active',
    width: 100,
    Cell: ({ original }) => (
      <div className="center">
        <Tooltip title="Редактировать">
          <Button
            className="ml1"
            type={'link'}
            size={'small'}
            color="primary"
            icon={'edit'}
            onClick={() => modalOnOpen(original)}
          />
        </Tooltip>
      </div>
    )
  };

  hasAccess(PERMISSIONS.ROLES_EDIT) && columns.push(actions);

  return columns;
};
