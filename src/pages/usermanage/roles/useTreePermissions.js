import { useState } from 'react';
import lodash from 'lodash';
import { PERMISSIONS, PERMISSIONS_TREE } from '../../../_helpers/Permissions';

const useTreePermissions = data => {
  const [permissions, setPermissions] = useState(data ? data.access_list : []);

  const allPermissionTotal = () => {
    const allPermissions = [];
    Object.values(PERMISSIONS_TREE).forEach(item => item.forEach(per => allPermissions.push(per)));
    return allPermissions.length + Object.keys(PERMISSIONS_TREE).length;
  };

  const onClickHandle = data => {
    if (PERMISSIONS_TREE.hasOwnProperty(data)) {
      if (permissions.includes(data)) {
        setPermissions([
          ...permissions.filter(
            item => !PERMISSIONS_TREE[data].includes(item) && data !== item && item !== 'all'
          )
        ]);
      } else {
        const newPermissions = [...permissions, ...PERMISSIONS_TREE[data], data];
        const isAll = newPermissions.length === allPermissionTotal();
        setPermissions(
          isAll ? [...permissions, ...PERMISSIONS_TREE[data], data, 'all'] : newPermissions
        );
      }
    } else if (data === 'all') {
      permissions.includes('all')
        ? setPermissions([])
        : setPermissions([...Object.values(PERMISSIONS), 'all']);
    } else {
      const isAll = allPermissionTotal() === [...permissions, data].length;
      const parent = lodash.findKey(PERMISSIONS_TREE, val => val.includes(data));
      const newPermissions = permissions.includes(parent)
        ? [...permissions]
        : [...permissions, parent];
      newPermissions.includes(data)
        ? setPermissions(
            newPermissions.filter(permission => permission !== data && permission !== 'all')
          )
        : setPermissions(isAll ? [...newPermissions, data, 'all'] : [...newPermissions, data]);
    }
  };

  const onChecked = ({ checked: list }) => {
    console.log(list,permissions)
    const setPermission = list.find(item => !permissions.includes(item));
    const removePermission = permissions.find(item => !list.includes(item));
    onClickHandle(setPermission || removePermission);
  };

  return { onChecked, onClickHandle, permissions };
};

export default useTreePermissions;
