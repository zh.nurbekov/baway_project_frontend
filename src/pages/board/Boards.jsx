import React, { useEffect, useState } from 'react';
import { Paper } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Wrapper } from './BoardStyles';
import Board from 'react-trello';
import { boardModule, changeStatus, loadStatements } from './BoardDucks';
import StatementDescriptionModal from '../Statement/StatementDescriptionModal';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import { localSecureStorage } from '../../_app/App';
import { Badge } from 'antd';

export default function Boards() {
  const lanes = [
    {
      id: 1,
      title: (
        <div style={{ fontSize: 18, fontWeight: 'bold', marginTop: 10, marginBottom: 10 }}>
          Делать
        </div>
      ),
      style: { width: 400, backgroundColor: '#9f9494', borderRadius: 8, minHeight: 300 },
      cardStyle: { backgroundColor: '#fff', height: 120, width: '100%', maxWidth: '100%' },
      cards: []
    },
    {
      id: 2,
      style: {
        width: 400,
        backgroundColor: '#3f495f',
        color: '#fff',
        borderRadius: 8,
        height:'100%'
      },
      title: (
        <div style={{ fontSize: 18, fontWeight: 'bold', marginTop: 10, marginBottom: 10 }}>
          В процессе работы
        </div>
      ),
      cardStyle: { backgroundColor: '#fff', height: 120, width: '100%', maxWidth: '100%' },
      cards: []
    },
    {
      id: 3,
      style: { width: 400, background: 'darkseagreen', borderRadius: 8, minHeight: 300 },
      title: 'Завершено',
      cardStyle: { backgroundColor: '#fff', height: 120, width: '100%', maxWidth: '100%' },
      cards: []
    }
  ];

  const { statements } = useSelector(state => state[boardModule]);
  const [data, setData] = useState({ lanes });
  const dispatch = useDispatch();
  const modal = useSimpleModal();
  const user = localSecureStorage.getItem('user');

  useEffect(() => {
    dispatch(loadStatements({}));
  }, []);

  useEffect(() => {
    const lineList = [...lanes];
    statements.map(item => {
      const title = item.organization;
      const description = (
        <div>
          <div className="p1">{item.phone}</div>
          {item.employerId && (
            <div className="flex justify-end">
              <Badge
                count={`${item.employerId.firstname} ${item.employerId.lastname}`}
                style={{
                  backgroundColor: '#001529',
                  color: '#fff',
                  padding: 15,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'end'
                }}
              />
            </div>
          )}
        </div>
      );
      if (!item.status || item.status.id === 1) {
        lineList[0].cards.push({ ...item, title, description });
      } else {
        if (item.status.id === lineList[1].id) {
          lineList[1].cards.push({ ...item, title, description });
        }
        if (item.status.id === lineList[2].id) {
          lineList[2].cards.push({ ...item, title, description });
        }
      }
    });
    setData({ lanes: lineList });
  }, [statements]);

  return (
    <Wrapper>
      <h3 children={'Рабочий процесс'} className="mb2 bold" />
      <Paper style={{ padding: 10, borderRadius: 0 }}>
        <div>
          <Board
            handleDragEnd={(statementId, _, statusId) => {
              dispatch(changeStatus(statusId, statementId, user));
            }}
            onCardClick={(cardId, metadata, laneId) => {
              console.log(cardId, metadata, laneId);
              const statement = statements.find(({ id }) => id === cardId);
              statement && modal.open(statement);
            }}
            data={data}
            style={{
              minHeight: 400,
              height: '100%',
              backgroundColor: '#fff',
              overflow: 'auto'
            }}
            cardStyle={{
              weight: 300
            }}
          />
        </div>
      </Paper>
      {modal.isOpen && <StatementDescriptionModal {...modal} />}
    </Wrapper>
  );
}
