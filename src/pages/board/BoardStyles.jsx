import styled from "styled-components";

export const Wrapper = styled.div`
  
  padding-bottom: 20px;
  .MuiPaper-elevation1 {
    box-shadow: 0px 1px 1px -1px rgb(0 0 0 / 20%), 0px 0px 0px 0px rgb(0 0 0 / 14%),
      0px 1px 5px 0px rgb(0 0 0 / 12%) !important;
  }

  .smooth-dnd-container *{
    height: 100%;
    overflow: hidden;
    max-height: 100%;
    
  }
`;
