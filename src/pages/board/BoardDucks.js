import { createReducer } from '@reduxjs/toolkit';
import { BoardApi } from './BoardService';
import statusMessage from '../../utils/statusMessage';

/**
 * Constants
 */

export const boardModule = 'board';
export const STATEMENT = `${boardModule}/STATEMENT`;
export const STATEMENT_CHANGE = `${boardModule}/STATEMENT_CHANGE`;
export const LOADING = `${boardModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  statements: [],
  loading: false
};

export default createReducer(initialState, {
  [STATEMENT]: (state, { payload }) => {
    state.statements = payload;
    state.loading = false;
  },
  [STATEMENT_CHANGE]: (state, { payload: { statusId, statementId, employerId } }) => {
    state.statements = [
      ...state.statements.map(item => {
        return item.id === statementId ? { ...item, status: { id: statusId }, employerId } : item;
      })
    ];
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadStatements = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await BoardApi.loadStatement(filter);
    dispatch({ type: STATEMENT, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const changeStatus = (statusId, statementId, user) => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await BoardApi.changeStatus(statusId, statementId);
    const employerId = !statusId || statusId === 1 ? null : user.id;
    await BoardApi.changeEmployee({ statementId, employerId });
    console.log(employerId,user)
    const success = () => {
      dispatch({
        type: STATEMENT_CHANGE,
        payload: { statusId, statementId, employerId: employerId ? user : null }
      });
    };
    statusMessage(data, success);
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};
