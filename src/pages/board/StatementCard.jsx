import React from 'react';
import { Paper } from '@material-ui/core';

export default function StatementCard({ data }) {
  console.log(data);
  return (
    <div>
      <Paper style={{ padding: 10, marginTop: 10, borderRadius: 0 }}>
        <h6>{data.organization}</h6>
      </Paper>
    </div>
  );
}
