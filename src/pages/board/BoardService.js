import { Api } from '../../_helpers/service';

export const BoardApi = {
  loadStatement: params => Api.get('api/statements', { params }),
  changeEmployee: values => Api.put('api/change/statement/employer',values ),
  changeStatus: (statusId, statementId) =>
    Api.put(`api/change/statement/status/${statusId}/${statementId}`)
};
