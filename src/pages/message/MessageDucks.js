import { createReducer } from '@reduxjs/toolkit';
import { MessageApi } from './MessageService';
import { localSecureStorage } from '../../_app/App';

/**
 * Constants
 */

export const messageModule = 'message';
export const USERS = `${messageModule}/USERS`;
export const CHAT_MESSAGE = `${messageModule}/CHAT_MESSAGE`;
export const ADD_CHAT_MESSAGE = `${messageModule}/ADD_CHAT_MESSAGE`;
export const CHAT_USER_LIST = `${messageModule}/CHAT_USER_LIST`;
export const MESSAGE_IS_READ = `${messageModule}/MESSAGE_IS_READ`;
export const COUNT_TOTAL_UNREAD = `${messageModule}/COUNT_TOTAL_UNREAD`;
export const NOTIFICATIONS = `${messageModule}/NOTIFICATIONS`;
export const NOTIFICATIONS_IS_READ = `${messageModule}/NOTIFICATIONS_IS_READ`;
export const CREATE = `${messageModule}/CREATE`;
export const LOADING = `${messageModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  users: [],
  chatMessage: [],
  countTotalUnread: 0,
  chatUsers: [],
  notifications:[],
  loading: false
};

export default createReducer(initialState, {
  [USERS]: (state, { payload }) => {
    state.users = payload;
    state.loading = false;
  },
  [CHAT_MESSAGE]: (state, { payload }) => {
    state.chatMessage = payload;
  },
  [ADD_CHAT_MESSAGE]: (state, { payload }) => {
    state.chatMessage = [...state.chatMessage, payload];
  },
  [MESSAGE_IS_READ]: (state, { payload }) => {
    state.chatUsers = [
      ...state.chatUsers.map(item => {
        if (item.senderId === payload) {
          state.countTotalUnread = state.countTotalUnread - item.countUnread;
          return { ...item, countUnread: 0 };
        }
        return item;
      })
    ];
  },
  [CHAT_USER_LIST]: (state, { payload }) => {
    state.chatUsers = payload;
  },
  [COUNT_TOTAL_UNREAD]: (state, { payload }) => {
    state.countTotalUnread = payload;
  },
  [NOTIFICATIONS]: (state, { payload }) => {
    console.log(payload)
    state.notifications = payload;
  },
  [NOTIFICATIONS_IS_READ]: (state, { payload }) => {
    state.notifications = [];
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadUsers = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let user = localSecureStorage.getItem('user');
    let { data } = await MessageApi.getUsers(filter);
    const users = data.data.map(item => {
      const title = `${item.firstname} ${item.middlename}`;
      return { ...item, title };
    });
    dispatch({ type: USERS, payload: users.filter(({id})=>id !== user.id) });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const loadChat = (userId, senderId) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOADING, loading: true });
    const { users } = getState()[messageModule];
    let { data } = await MessageApi.loadChat(userId, senderId);
    let user = localSecureStorage.getItem('user');

    const messages = data.map(item => {
      const sender = users.find(({ id }) => id === item.senderId);
      console.log(sender,users,data)
      return {
        ...item,
        text: item.message,
        sender: {
          ...sender,
          name: sender ? `${sender.firstname} ${sender.lastname }`:"",
          uid: user.id === item.senderId ? 'user1' : 'user2',
          avatar:
            'https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png'
        }
      };
    });

    dispatch({ type: MESSAGE_IS_READ, payload: senderId });
    dispatch({ type: CHAT_MESSAGE, payload: messages });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const sendMessage = values => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await MessageApi.sendMessage(values);
    let sender = localSecureStorage.getItem('user');
    const message = {
      ...values,
      id: 100000,
      text: values.message,
      sender: {
        ...sender,
        name: `${sender.firstname} ${sender.lastname}`,
        uid: 'user1',
        avatar:
          'https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png'
      }
    };

    dispatch({ type: ADD_CHAT_MESSAGE, payload: message });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const loadCheckMessage = userId => async (dispatch, getState) => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await MessageApi.loadCheckMessage(userId);
    dispatch({ type: CHAT_USER_LIST, payload: data.data });
    dispatch({ type: COUNT_TOTAL_UNREAD, payload: data.countTotalUnread });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const loadNotifications = userId => async (dispatch, getState) => {
  try {
    dispatch({ type: LOADING, loading: true });
    let data  = await MessageApi.loadNotifications(userId);
    dispatch({ type: NOTIFICATIONS, payload: data.data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const notificationsIsRead = userId => async (dispatch, getState) => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await MessageApi.notificationsIsRead(userId);
    console.log(data)
    dispatch({ type: NOTIFICATIONS, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};