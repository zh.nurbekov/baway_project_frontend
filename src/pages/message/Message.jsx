import React, { useEffect, useState } from 'react';
import { Paper } from '@material-ui/core';
import { ChatBox } from 'react-chatbox-component';
import { AutoComplete, Avatar, Badge, Empty, Icon, Input, List } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { loadChat, loadCheckMessage, loadUsers, messageModule, sendMessage } from './MessageDucks';
import { Wrapper } from './MessageStyles';
import { localSecureStorage } from '../../_app/App';
import Select from '../../_ui/Select';

const messages = [
  {
    text: 'Добрый день',
    id: '1',
    sender: {
      name: 'Гульназ',
      uid: 'user1',
      avatar:
        'https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png'
    }
  },
  {
    text: 'Добрый день',
    id: '2',
    sender: {
      name: 'Дина',
      uid: 'user2',
      avatar:
        'https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png'
    }
  }
];

export default function Message() {
  const { users, chatMessage, chatUsers } = useSelector(state => state[messageModule]);
  const [firstname, setFirstName] = useState('');
  const [chatSenderId, setChatSenderId] = useState();
  const dispatch = useDispatch();
  let user = localSecureStorage.getItem('user');


  console.log(user)

  useEffect(() => {
    dispatch(loadCheckMessage(user.id));

    dispatch(loadUsers());
  }, []);

  useEffect(() => {
    dispatch(loadChat(user.id, chatSenderId));
  }, [chatSenderId]);

  const sendMessages = message => {
    const mail = {
      user: user.id,
      recipient: chatSenderId,
      message
    };

    dispatch(sendMessage(mail));
  };

  return (
    <Wrapper>
      <h3 children={'Сообщения'} className="mb2 bold" />
      <Paper style={{ padding: 10 }}>
        <div className="md-col-12 flex">
          <div className="md-col-8">
            <div className="container ">
              {chatSenderId && <ChatBox messages={chatMessage} onSubmit={sendMessages} />}
            </div>
          </div>
          <div className="md-col-4 pr2">
            <Select
              suffixIcon={<Icon type="search" />}
              placeholder={'Поиск'}
              value={chatSenderId}
              showSearch
              withoutForm
              style={{ width: 200 }}
              onChange={setChatSenderId}
              options={users.map(({ id: value, firstname, middlename }) => ({
                value,
                name: `${firstname} ${middlename}`
              }))}
            />
            {chatUsers.length ? (
              chatUsers.map(item => (
                <Paper
                  onClick={() => setChatSenderId(item.senderId)}
                  style={{
                    marginTop: 10,
                    padding: 10,
                    borderRadius: 8,
                    border: '1px solid #756f6f'
                  }}
                >
                  <div className="flex justify-between">
                    <div className="flex items-center cursor-pointer">
                      <img
                        width={30}
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1iCQBweynU5envqe_ENa1z0MVUp7uENqj2MtxkImbwNQiIVAtpTMMt6OiRG9nrsYva6c&usqp=CAU"
                      />
                      <h6 style={{ margin: 0, marginLeft: 10 }}>{item.senderFullName}</h6>
                    </div>
                    <div>
                      <Badge
                        count={item.countUnread}
                        style={{
                          width: 30,
                          backgroundColor: '#6f9d94',
                          color: '#f6f6f6',
                          boxShadow: '0 0 0 1px #d9d9d9 inset'
                        }}
                      />
                    </div>
                  </div>
                </Paper>
              ))
            ) : (
              <Empty description={'не найдено'} style={{ marginTop: 30 }} />
            )}
          </div>
        </div>
      </Paper>
    </Wrapper>
  );
}
