import React from 'react';

export default function Info() {
  return (
    <div style={{ width: '100%', color: 'black', textAlign: 'center' }}>
      <h5 className="flex justify-center my2">Наше настоящее</h5>
      <div className="flex justify-center fs-14">
        <div style={{ textAlign: 'center' }} className=" flex justify-center col-10">
          Утилизирующая компания "Урал" уже долгое время успешно работает на рынке профессиональной
          утилизации всевозможных видов и типов оборудования, техники, электроники, а также мебели.
          <br />
          <br /> С самого начала нашей деятельности и по настоящее время Утилизирующая компания
          «Урал» при реализации своих проектов использовала комплексный подход и была
          клиентоориентированна.
          <br />
          <br />
          За время нашей работы нам удалось накопить огромный профессиональный опыт, существенно
          расширить перечень выполняемых работ и завоевать доверие огромного числа клиентов на
          территории нашей страны.
          <br /> <br />
          Утилизирующая компания «Урал» работает как с юридическими, так и с физическими лицами, как
          с коммерческими организациями, так и государственными структурами. Нашей целью является
          построение долгосрочных отношений с заказчиками, и поэтому мы организовали нашу работу
          так, чтобы каждый обращающийся к нам клиент получал наибольшую выгоду от сотрудничества с
          нами.
          <br /> <br />
          Наша работа основывается на принципах законности и правомерности на территории всех
          основных субъектов Российской Федерации, что подтверждается соответствующими лицензиями и
          сертификатами.
        </div>
      </div>
      <div className='flex justify-center fs-13'>
        <div className="my3 col-8 p2">
          <h6 className="flex justify-center mb2">Наши перспективы</h6>
          <div>
            Утилизирующая компания «Урал» продолжает расти и развиваться. Однако, несмотря на
            заслуженное кропотливым и упорным трудом доверие и уважение, мы не стоим на месте.
            <br />
            <br />
            Утилизирующая компания «Урал» постепенно увеличивает перечень оказываемых услуг,
            находятся все новые способы сотрудничества с заказчиками и партнерами.
            <br />
            <br />
            Звоните, и мы с радостью решим все Ваши проблемы, связанные с утилизацией. Обратившись к
            нам один раз, Вы останетесь с нами навсегда, ведь сотрудничество с Утилизирующей
            компанией "Урал" – это неизменное качество предоставляемых услуг, высокая скорость
            выполнения работ, демократичные цены и 100% гарантии!
          </div>
        </div>
      </div>
    </div>
  );
}
