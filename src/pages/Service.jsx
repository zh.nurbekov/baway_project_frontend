import React from 'react';
import { Paper } from '@material-ui/core';
import paths from '../_helpers/paths';
import { history } from '../_helpers/store';

export default function Service() {
  return (
    <div className=" width_100">
      <Paper style={{ padding: 40 }}>
        <div className="flex col-12 center mb2">
          <div className="col-4">
            <img
              width={100}
              height={100}
              style={{ objectFit: 'cover', cursor: 'pointer' }}
              onClick={() => history.push(paths.createStatement)}
              src="/service_1.jpeg"
              alt=""
            />
            <div className="mt1">
              Утилизация оргтехники и <br /> офисной техники
            </div>
          </div>
          <div className="col-4">
            <img
              width={100}
              height={100}
              style={{ objectFit: 'fill', cursor: 'pointer' }}
              onClick={() => history.push(paths.createStatement)}
              src="/service2.jpeg"
              alt=""
            />
            <div className="mt1">
              Утилизация <br />
              бытовой техники
            </div>
          </div>
          <div className="col-4">
            <img
              width={100}
              height={100}
              style={{ objectFit: 'fill', cursor: 'pointer' }}
              onClick={() => history.push(paths.createStatement)}
              src="/service3.jpeg"
              alt=""
            />
            <div className="mt1">
              Утилизация
              <br />
              медицинского
              <br />
              оборудования
            </div>
          </div>
        </div>
        <div className="flex col-12 center mt2">
          <div className=" col-6">
            <img
              width={100}
              height={100}
              style={{ objectFit: 'cover', cursor: 'pointer' }}
              onClick={() => history.push(paths.createStatement)}
              src="/service5.jpeg"
              alt=""
            />
            <div className="mt1">
              Утилизация <br />
              мебели
            </div>
          </div>
          <div className="col-6">
            <img
              width={100}
              height={100}
              style={{ objectFit: 'cover', cursor: 'pointer' }}
              onClick={() => history.push(paths.createStatement)}
              src="/service4.jpeg"
              alt=""
            />
            <div className="mt1">
              Утилизация
              <br />
              банковского
              <br />
              оборудования
            </div>
          </div>
        </div>
      </Paper>
    </div>
  );
}
