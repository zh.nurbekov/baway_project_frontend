export default function getOptions(list) {
  return list
    ? list.map(({ id, name }) => ({
        value: id,
        name
      }))
    : [];
}
