import { connect } from 'react-redux';
import withLocalization from "../_hoc/withLocalization";

export default function(mapStateToProps, mapDispatchToProps) {
  if (typeof mapDispatchToProps === 'function') {
    mapDispatchToProps = mapDispatchToProps();
  }
  return Component => withLocalization(connect(
    mapStateToProps || null,
    mapDispatchToProps
  )(Component))
}
