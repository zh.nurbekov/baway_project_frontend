import SecureLS from "secure-ls";

export const UseLocalSecureStorage = (options = {encodingType: 'aes', isCompression: false}) => {
    const ls = new SecureLS(options);

    const getItem = key => {
        try {
            return ls.get(key)
        } catch (e) {
            return null
        }
    };

    const setItemToLocalStorage = (key, data) => {
        try {
            let newData = JSON.stringify(data)
            ls.setDataToLocalStorage(key, newData)
        } catch (e) {
            return null
        }
    }

    const getItemFromLocalStorage = (key) => {
        try {
            let result = JSON.parse(ls.getDataFromLocalStorage(key))
            return result
        } catch (e) {
            return null
        }
    }

    return {
        setItem: (key, data) => ls.set(key, data),
        getItem: key => getItem(key),
        removeItem: key => ls.remove(key),
        clear: () => ls.removeAll(),
        setItemToLocalStorage: (key, data) => setItemToLocalStorage(key, data),
        getItemFromLocalStorage: key => getItemFromLocalStorage(key),
        removeLocalStorageItem: key => localStorage.removeItem(key),
        clearAllLocalStorage: () => ls.clear(),
        getAllKeys: () => ls.getAllKeys(),
    }
}