/**
 * Получаем справочники и сохраняем в localSecureStorage
 */

import {localSecureStorage} from "../_app/App";

export function handbooks(type) {
  return type
    ? localSecureStorage.getItem('handbook')[type]
    : localSecureStorage.getItem('handbook');
}
export function handbookItem(type, value) {
  const handbook = localSecureStorage.getItem('handbook')[type];
  return handbook.find(item => item.id === value);
}

export function handbookItemValue(type, value) {
  const handbook = localSecureStorage.getItem('handbook')[type];
  const item = handbook.find(item => item.id === value);
  return item ? item.value : '';
}
