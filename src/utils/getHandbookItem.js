import { findIndex } from 'lodash';

export default function(key, handbook, item) {
  return handbook[key][findIndex(handbook[key], ['id', item])];
}
