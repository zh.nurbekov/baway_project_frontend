export default function(obj, key) {
  return obj ? obj[key] : '';
}
