export default function(filter, newFilter) {
  let result = { ...filter, ...newFilter };
  if (newFilter && !newFilter.hasOwnProperty('offset')) {
    result.offset = 0;
  }
  for (let item of Object.entries(result)) {
    if (item[1] === '' || item[1] === 'Invalid date' || item[1] === null) {
      delete result[item[0]];
    }
  }

  return result;
}
