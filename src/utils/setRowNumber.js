export default function setRowNumber(data) {
  let i=1;
  for (let item of data) {
      item.rowNumber = i;
      i++
  }
  return data;
}
