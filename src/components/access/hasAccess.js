import React from 'react';
import {localSecureStorage} from "../../_app/App";

export default function(permissions, accessible) {
  const user = localSecureStorage.getItem('user');
  if (user && user.accessList) {
    let isAccess = false;
    if (accessible) {
      isAccess = true;
    } else if (user && user.accessList) {
      if (permissions instanceof Array) {
        if (user.accessList.some(item => permissions.includes(item))) isAccess = true;
      } else {
        if (user.accessList.includes(permissions)) isAccess = true;
      }
    }
    return isAccess || null;
  } else {
    return null;
  }
}
