import React from 'react';
import { localSecureStorage } from '../../_app/App';

function Access({ permissions, accessible = false, children }) {
  const user = localSecureStorage.getItem('user');
  let isAccess = false;
  if (accessible) {
    isAccess = true;
  } else if (user && user.accessList) {
    if (permissions instanceof Array) {
      if (user.accessList.some(item => permissions.includes(item))) isAccess = true;
    } else {
      if (user.accessList.includes(permissions)) isAccess = true;
    }
  }

  return isAccess ? <>{children}</> : null;
}

export default Access;
