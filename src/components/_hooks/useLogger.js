import {useEffect} from "react";

function useLogger(value, name = 'Value') {
    useEffect(()=> {
        console.log(`${name} changed:`, value)
    }, [value])

}
export default useLogger;