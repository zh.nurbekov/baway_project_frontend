import { useState } from 'react';

function useSimpleModal() {
  const [data, setData] = useState(null);
  return {
    data,
    isOpen: !!data,
    open: value => setData(value || {}),
    dataIsEmpty: data && Object.values(data).length <= 0,
    close: () => setData(null)
  };
}

export default useSimpleModal;
