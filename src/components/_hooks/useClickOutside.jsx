import React, { useEffect, useState } from 'react';
import * as ReactDOM from 'react-dom';

function useClickOutside(className ) {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => document.removeEventListener('click', handleClickOutside, true);
  }, []);

  const handleClickOutside = event => {
    const domNode = ReactDOM.findDOMNode(this);
    if (!domNode || !domNode.contains(event.target)) {
      const isShow = event.path.filter(
        item => item.className && item.className.toString().indexOf(className) !== -1
      );
      isShow.length === 0 && setIsOpen(false);
    }
  };

  return { isOpen, setIsOpen };
}

export default useClickOutside;
