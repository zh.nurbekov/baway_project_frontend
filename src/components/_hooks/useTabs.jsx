import React, { useEffect, useState } from 'react';
import { history } from '../../_helpers/store';
import { localSecureStorage } from '../../_app/App';

const tabKeyUrl = ':tab';
function useTabs(match, tabList, url) {
  const [tab, setTab] = useState(null);

  useEffect(() => {
    const test = getActiveKey();
    setTab(test);
  }, []);

  const changeTab = key => {
    setTab(key);
    history.push(url.replace(tabKeyUrl, key));
  };

  function getActiveKey() {
    const tabs = getTabs();
    if (match.params.tab === tabKeyUrl) {
      if (tabs.length) {
        history.push(url.replace(tabKeyUrl, tabs[0].key));
        return tabs[0].key;
      } else return null;
    } else {
      return tabs.length ? tabs.find(item => item.key === match.params.tab).key : null;
    }
  }

  function getTabs() {
    const user = localSecureStorage.getItem('user');
    if (user && user.accessList) {
      const newTabs = tabList.filter(item =>
        item.permissions.some(ps => user.accessList.includes(ps))
      );
      return newTabs;
    } else {
      return [];
    }
  }

  return { changeTab, tab, getTabs };
}

export default useTabs;
