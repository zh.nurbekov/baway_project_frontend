import { createReducer } from "@reduxjs/toolkit";
import { RU_TRANSLATION } from "../../_locale/ru";
import { KK_TRANSLATION } from "../../_locale/kk";
import { EN_TRANSLATION } from "../../_locale/en";
import {localSecureStorage} from "../../_app/App";

/**
 * Constants
 */

export const localeModule = "locale";
export const CHANGE = `${localeModule}/CHANGE`;


/**
 * Reducer
 */


const initialState = {
  lang: RU_TRANSLATION.lang,
  messages: RU_TRANSLATION.messages
};

export default createReducer(initialState, {
  [CHANGE]: (state, action) => ({
    lang: action.lang,
    messages: action.messages
  })
});



/**
 * Actions
 */


export const changeLocale = lang => {
  localSecureStorage.setItem("lang", lang);

  let messages = RU_TRANSLATION.messages;
  if (lang === "kk") messages = KK_TRANSLATION.messages;
  if (lang === "en") messages = EN_TRANSLATION.messages;

  return {
    type: CHANGE,
    lang: lang,
    messages: messages
  };
};
