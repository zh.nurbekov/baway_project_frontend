import { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

export const DEFAULT_FILTER = { offset: 0, limit: 10 };

let initialRequest = false;
export default function useTableFilter({
  loadData,
  index = '',
  filterParam,
  addToRequestParam,
  total
}) {
  const dispatch = useDispatch();
  const filterName = `filter${index}`;
  const orderName = `order${index}`;
  const [tableFilter, setTableFilter] = useState(null);
  const [localTotal, setLocalTotal] = useState(total);
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const params = new URLSearchParams(search);
  const defaultFilter = filterParam ? { ...DEFAULT_FILTER, ...filterParam } : DEFAULT_FILTER;
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : defaultFilter;
  const order = params.get(orderName) ? JSON.parse(params.get(orderName)) : {};

  const reset = () => {
    params.set(filterName, JSON.stringify(defaultFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  const setFilter = param => {
    let requestFilter = { ...filter, ...param };
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  useEffect(() => {
    let requestFilter = { ...defaultFilter, ...filter };
    if (Object.keys(order).length > 0) requestFilter.sorting = order;
    if (JSON.stringify(requestFilter) !== JSON.stringify(tableFilter)) {
      if (tableFilter === null) {
        setFilter(requestFilter);
      } else if (checkPagination()) {
        requestFilter.offset = 0;
        setFilter(requestFilter);
      }
      dispatch(loadData(requestFilter, addToRequestParam));
      setTableFilter({ ...requestFilter });
    }
    setLocalTotal(total);
  }, [search]);

  useEffect(() => {
    if (JSON.stringify(tableFilter) === JSON.stringify(filter)) {
      const currentTotal = localTotal - total;

      if (currentTotal === 1 || currentTotal === -1) {
        dispatch(loadData(filter, addToRequestParam));
      }
    }
    setLocalTotal(total);
  }, [total]);

  const clearFilter = () => () => {
    if (window.location.search) {
      if (pathname === window.location.pathname) {
        const params = new URLSearchParams(search);
        params.delete(filterName);
        params.delete(orderName);
        replace(`${pathname}?${params.toString()}`);
      }
    }
  };
  useEffect(() => clearFilter(), []);

  const checkPagination = () => {
    const { offset } = tableFilter;
    return offset === filter.offset;
  };

  return { tableFilter, order, reset, index, setFilter };
}
