import { useState, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

const defaultPageParams = { offset: 0, limit: 10 };
function usePaginationFilter(data, queryFilterName = '', loadData, total) {
  const dispatch = useDispatch();
  const filterName = `filter${queryFilterName}`;
  const [pageParam, setPageParam] = useState(getParams());
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const params = new URLSearchParams(search);
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : defaultPageParams;

  useEffect(() => {
    setPageParam(getParams());
  }, [search]);

  useEffect(() => {
    const count = pageParam.page * pageParam.size + data.length;
    const filterIsLoad = queryFilterName && params.get(filterName);
    if (data.length && data.length < pageParam.size && total > count && filterIsLoad) {
      dispatch(loadData(filter));
    }
  }, [data]);

  const changePaginationPage = page => {
    const offset = page * filter.limit;
    let requestFilter = { ...defaultPageParams, ...filter, offset };
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  const changePaginationLimit = limit => {
    let requestFilter = { ...defaultPageParams, ...filter, offset: 0, limit };
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  function getParams() {
    let requestFilter = { ...defaultPageParams, ...filter };
    const page = requestFilter.offset / requestFilter.limit;
    return { page, size: requestFilter.limit };
  }

  return { pageParam, changePaginationPage, changePaginationLimit, filter };
}

export default usePaginationFilter;
