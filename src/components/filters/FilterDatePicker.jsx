import React from 'react';

import { DatePicker } from 'antd';
import moment from 'moment';
import useColumnFilter from './useColumnFilter';

function FilterDatePicker({ name, queryFilterName = '', placeholder, style }) {
  const { value, setFilter } = useColumnFilter(name, queryFilterName);

  const getValue = () => {
    let result = undefined;
    if (value) {
      result = moment.unix(value);
    }
    return result;
  };

  return (
    <div style={{ ...style }}>
      <DatePicker
        style={{ minWidth: 180 }}
        placeholder={placeholder}
        format="YYYY-MM-DD HH:mm:ss"
        value={getValue()}
        onChange={(moment, value) => setFilter(moment.unix())}
      />
    </div>
  );
}

export default FilterDatePicker;
