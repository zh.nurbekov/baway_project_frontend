import { useHistory, useLocation } from 'react-router-dom';
import { useEffect } from 'react';

export const useClearFilter = index => {
  const filterName = `filter${index}`;
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const orderName = `order${index}`;

  useEffect(() => {
    if (window.location.search) {
      if (pathname === window.location.pathname) {
        const params = new URLSearchParams(search);
        params.delete(filterName);
        params.delete(orderName);
        replace(`${pathname}?${params.toString()}`);
      }
    }
  }, []);

};
