import { useLocation, useHistory } from 'react-router-dom';

const DEFAULT_FILTER = { limit: 10, offset: 0 };
export default function useQueryFilter({ queryFilterName = '', filterParam }) {
  const filterName = `filter${queryFilterName}`;
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const params = new URLSearchParams(search);
  const defaultFilter = filterParam ? { ...filterParam, ...DEFAULT_FILTER } : DEFAULT_FILTER;
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : defaultFilter;

  const reset = () => {
    params.set(filterName, JSON.stringify(defaultFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  const setFilter = param => {
    let requestFilter = { ...filter, ...param };
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  return { reset, setFilter, filter };
}
