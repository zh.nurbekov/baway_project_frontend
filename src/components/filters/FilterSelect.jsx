import React from 'react';
import useColumnFilter from './useColumnFilter';
import { Select } from 'antd';
const { Option } = Select;

function FilterSelect({
  name,
  options,
  queryFilterName = '',
  placeholder,
  style,
  width = 220,
  label,
  ...props
}) {
  const { value, setFilter } = useColumnFilter(name, queryFilterName);

  return (
    <div style={{ ...style, width: width }}>
      <span children={label} />
      <Select
        style={{ width: '100%' }}
        allowClear
        value={value}
        placeholder={placeholder}
        onChange={value => setFilter(value)}
        {...props}
      >
        {options.map(({ value, label }) => (
          <Option key={value} value={value}>
            {label}
          </Option>
        ))}
      </Select>
    </div>
  );
}

export default FilterSelect;
