import React, { useEffect, useState } from 'react';
import { Input } from 'antd';
import useColumnFilter from './useColumnFilter';
import styled from 'styled-components';

const Wrapper = styled.div`
  .ant-input-affix-wrapper .ant-input-suffix {
    right: 21px !important;
  }
`;

function FilterInput({
  name,
  queryFilterName = '',
  lengthToFind = 3,
  placeholder = 'Поиск',
  style,
  width = 220
}) {
  const filter = useColumnFilter(name, queryFilterName);
  const [timeOutId, registerTimeOut] = useState(null);
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(filter.value || '');
  }, [filter.value]);

  const handleChange = ({ target: { value } }) => {
    if (value.length < lengthToFind) {
      filter.setFilter(undefined);
      clearTimeout(timeOutId);
      setValue(value);
    } else {
      clearTimeout(timeOutId);
      registerTimeOut(
        setTimeout(() => {
          filter.setFilter(value);
        }, 1000)
      );
      setValue(value);
    }
  };

  return (
    <Wrapper style={style}>
      <Input
        value={value}
        style={{ width: width }}
        placeholder={placeholder}
        allowClear
        onChange={handleChange}
      />
    </Wrapper>
  );
}

export default FilterInput;
