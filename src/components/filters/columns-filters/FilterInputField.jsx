import React from 'react';
import { Input } from 'antd';

const FilterInputField = ({ placeholder, styles }) => {
  return ({ filter, onChange }) => {
    return (
      <div style={styles}>
        <Input
          placeholder={placeholder || ''}
          onChange={({ target: { value } }) => onChange(value)}
          value={filter ? filter.value : ''}
        />
      </div>
    );
  };
};

export default FilterInputField;
