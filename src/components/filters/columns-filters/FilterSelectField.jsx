import { Select } from 'antd';
import React from 'react';
const { Option } = Select;

const FilterInputField = ({ options, styles }) => {
  const getOptions = option => {
    return [
      <Option key={0} value={''}>
        {'Все'}
      </Option>,
      ...option.map(item => (
        <Option key={item.value} value={item.value}>
          {item.label}
        </Option>
      ))
    ];
  };
  return ({ filter, onChange }) => {
    return (
      <div style={styles}>
        <Select
          style={{ width: 100 }}
          onChange={value => onChange(value)}
          value={filter ? filter.value : ''}
        >
          {getOptions(options)}
        </Select>
      </div>
    );
  };
};

export default FilterInputField;
