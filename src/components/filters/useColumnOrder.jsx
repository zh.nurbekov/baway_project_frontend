import useColumnFilter from './useColumnFilter';

function useColumnOrder(columnName, queryFilterName) {
  const { setFilter, value } = useColumnFilter(columnName, queryFilterName);

  const changeOrder = () => {
    if (!value) {
      setFilter('desc');
    } else if (value === 'desc') {
      setFilter('asc');
    } else if (value === 'asc') {
      setFilter('desc');
    }
  };

  return {
    order: value,
    setOrder: changeOrder
  };
}

export default useColumnOrder;
