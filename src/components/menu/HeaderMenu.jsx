import React, { useState } from 'react';
import { Button, Icon, Layout, Menu } from 'antd';
import { Contents, Wrapper } from './HeaderMenuStyles';
import routers from '../../_helpers/routers';
import { history } from '../../_helpers/store';
import paths from '../../_helpers/paths';
import { localSecureStorage } from '../../_app/App';
import SideBar from './SideBar';
import useSimpleModal from '../_hooks/useSimpleModal';
import { IconButton } from '@material-ui/core';
const { Header, Content, Footer } = Layout;

const withoutPadding = [paths.cableNetworks, paths.maps];

const HeaderMenu = ({ children }) => {
  const statementModal = useSimpleModal();
  let user = localSecureStorage.getItem('user');
  const path = window.location.pathname;

  console.log(user);
  const menu =
    user && user.accessList
      ? routers.filter(
          item =>
            item.sideBarItem &&
            item.permissions.some(permission => user.accessList.includes(permission))
        )
      : [];

  const selected = () => {
    const menuItem = menu.find(item => item.path.indexOf(path.split('/')[1]) !== -1);
    return menuItem ? menuItem.path : path;
  };

  return (
    <Layout style={{ width: '100%' }}>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <div className="flex items-center justify-between">
          {/*<Button icon="menu" type="link" />*/}
          {/*<Icon type="menu" className='cursor-pointer' />*/}
          <div className="flex items-center">
            {user && <SideBar />}
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={['2']}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item key="1" onClick={() => history.push(paths.home)} children={'Главная'} />
              <Menu.Item key="2" onClick={() => history.push(paths.info)}>
                О компании
              </Menu.Item>
              <Menu.Item key="3" onClick={() => history.push(paths.service)}>
                Услуги
              </Menu.Item>
              <Menu.Item
                key="5"
                onClick={() => history.push(paths.createStatement)}
                children={'Подача заявки'}
              />
              <Menu.Item key="6" onClick={() => history.push(paths.contacts)}>
                Контакты
              </Menu.Item>
            </Menu>
          </div>
          {user ? (
            <div className="flex items-center">
              <div>{`${user.firstname}  ${user.middlename}`}</div>
              <Button
                type={'link'}
                style={{ color: 'white' }}
                // onClick={() => history.push(paths.login)}
              >
                <Icon style={{ fontSize: 20 }} type="user" />
              </Button>
            </div>
          ) : (
            <Button
              type={'link'}
              style={{ color: 'white' }}
              onClick={() => history.push(paths.login)}
            >
              <Icon type="export" style={{ fontSize: 20 }} />
            </Button>
          )}
        </div>
      </Header>
      <Contents style={{ padding: '50px', marginTop: 64, width: '100%', minHeight: 700 }}>
        {children}
      </Contents>
    </Layout>
  );
};

export default HeaderMenu;
