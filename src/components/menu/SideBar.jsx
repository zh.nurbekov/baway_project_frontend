import { Badge, Divider, Drawer, Icon, Menu } from 'antd';
import React, { useState } from 'react';
import routers from '../../_helpers/routers';
import { history } from '../../_helpers/store';
import { useLocation } from 'react-router-dom';
import paths from '../../_helpers/paths';
import { useDispatch, useSelector } from 'react-redux';
import { messageModule } from '../../pages/message/MessageDucks';
import { localSecureStorage } from '../../_app/App';
import { logout } from '../../pages/login/LoginDucks';

export default function SideBar() {
  const { countTotalUnread } = useSelector(state => state[messageModule]);
  const { pathname } = useLocation();
  const [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();
  const user = localSecureStorage.getItem('user');
  return (
    <>
      <Icon
        style={{ fontSize: 20 }}
        type="menu"
        className="cursor-pointer mr2"
        onClick={() => setIsOpen(true)}
      />
      <Drawer
        width={250}
        title=""
        placement={'left'}
        onClose={() => setIsOpen(false)}
        visible={isOpen}
        bodyStyle={{ padding: 0 }}
      >
        {user && user.accessList && (
          <Menu style={{ marginTop: 40 }} selectedKeys={pathname}>
            {routers
              .filter(
                ({ sideBarItem, permissions }) =>
                  sideBarItem && permissions.some(item => user.accessList.includes(item))
              )
              .map(({ title, path, icon }) => (
                <Menu.Item key={path} children={title} onClick={() => history.push(path)}>
                  <div className="flex items-center width_100">
                    <div className="flex justify-between width_100">
                      <div className="flex items-center width_100">
                        {icon} {title}
                      </div>
                    </div>

                    {path === paths.message && (
                      <Badge
                        count={countTotalUnread}
                        style={{
                          width: 30,
                          backgroundColor: '#6f9d94',
                          color: '#f6f6f6',
                          boxShadow: '0 0 0 1px #d9d9d9 inset'
                        }}
                      />
                    )}
                  </div>
                </Menu.Item>
              ))}
            <Divider />
            <Menu.Item key={'exit'} onClick={() => dispatch(logout())}>
              <div className="flex items-center width_100">
                <div className="flex justify-between width_100">
                  <div className="flex items-center width_100">
                    <Icon type="export" />
                    <div children={'Выйти'} />
                  </div>
                </div>
              </div>
            </Menu.Item>
          </Menu>
        )}
      </Drawer>
    </>
  );
}
