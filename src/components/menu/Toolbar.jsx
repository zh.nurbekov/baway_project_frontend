import React from 'react';
import { Icon } from 'antd';
import { Layout } from 'antd';
import { history } from '../../_helpers/store';
import { useDispatch } from 'react-redux';
import { logout } from '../../pages/login/LoginDucks';
import paths from '../../_helpers/paths';
import routers from "../../_helpers/routers";
import {localSecureStorage} from "../../_app/App";

const { Header } = Layout;

const getThisSection = () => {
  const path = window.location.pathname;
  const thisSectionPathname = path.split('/')[1]
  const section = routers.find(section => {
    const routerPathname = section.path.split('/')[1]

    return thisSectionPathname === routerPathname
  })

  return section
}

function Toolbar({ collapsed, setCollapsed }) {
  const dispatch = useDispatch();
  let user = localSecureStorage.getItem('user');
  const thisSection = getThisSection()

  return (
    <Header>
      <div className="clearfix flex items-center width_100">
        <div className="col col-8 flex items-center justify-start">
          <Icon
            style={{ fontSize: 20 }}
            className="trigger"
            type={collapsed ? 'menu-unfold' : 'menu-fold'}
            onClick={() => setCollapsed(!collapsed)}
          />
          <div>{thisSection && thisSection.title}</div>
        </div>
        <div className="col col-4 flex items-center justify-end">
          <div style={{ marginRight: 15 }}>{user && user.full_name}</div>
          <Icon
            type="export"
            style={{ fontSize: 20, color: '#fff', marginRight: 36 }}
            onClick={() => {
              dispatch(logout());
              history.push(paths.login);
            }}
          />
        </div>
      </div>
    </Header>
  );
}

export default Toolbar;
