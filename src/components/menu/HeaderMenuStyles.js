import styled from 'styled-components';
import { Layout } from 'antd';

export const Wrapper = styled(Layout)`
  width: 100%;

  .ant-layout-header {
    display: flex;
    align-items: center;
    background-color: rgb(51, 51, 51);
    padding: 0;
    height: 50px;
    color: white;
  }

  .trigger {
    font-size: 20px;
    line-height: 50px;
    padding: 0 24px;
    cursor: pointer;
    color: white;
    transition: color 0.3s;
  }

  .trigger:hover {
    color: #1890ff;
  }

  .logo {
    height: 32px;
    margin: 9px;
  }

  .ant-layout-sider {
    background-color: rgb(255, 255, 255);
  }

  .ant-menu-dark,
  .ant-menu-dark .ant-menu-sub {
    background-color: rgb(255, 255, 255);
  }

  .ant-menu-inline {
    border: none !important;
  }
`;

export const Contents = styled.div`
  width: 100%;
  height: 100%;
  margin: 0;
`;
