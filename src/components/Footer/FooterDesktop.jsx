import React from 'react';
import Divider from '@material-ui/core/Divider';
import useWidthStyle from './useWidthStyle';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import { Wrapper } from './FooterStyle';
import MailIcon from '@material-ui/icons/Mail';
import PhoneIcon from '@material-ui/icons/Phone';

function FooterDesktop() {
  const widthStyle = useWidthStyle();
  return (
    <div>
      <Wrapper>
        <div
          className="px4 py3 flex items-center justify-between"
          style={{ color: 'white', paddingLeft: 100, paddingRight: 100 }}
        >
          <div className="flex items-center">
            <PersonPinIcon fontSize={'large'} style={{ color: 'white' }} />
            <div className="ml1">
              <div className="fs-13">г.Байконур , ул. Янгеля</div>
              <div className="fs-13">д. 2, кв. 24</div>
              <div className="fs-13"> Казахстан, 468320</div>
            </div>
          </div>
          <div>
            <div className="flex items-center mb1">
              <MailIcon style={{ color: 'white' }} />
              <div className="fs-13">ukural.ru@gmail.com</div>
            </div>
            <div className="flex items-center">
              <PhoneIcon style={{ color: 'white' }} />
              <div className="fs-13">+7(705)-246-22-98</div>
            </div>
          </div>
        </div>
      </Wrapper>
    </div>
  );
}

export default FooterDesktop;
