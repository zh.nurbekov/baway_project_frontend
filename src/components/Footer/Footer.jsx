import React from 'react';
import FooterDesktop from './FooterDesktop';
import { useTheme } from '@material-ui/core';

function Footer() {
  const theme = useTheme();
  return (
    <>
      <FooterDesktop />
    </>
  );
}

export default Footer;
