
export default function useWidthStyle() {

  return {
    width: '100%',
    maxWidth: 1748,
    paddingLeft: 80,
    paddingRight: 80,
    margin: 'auto',
  };
}
