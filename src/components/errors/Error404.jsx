import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100% !important;
  height: 100% !important;

  h2 {
    font-size: 28px;
    margin-bottom: 2em;
    color: #555 !important;
  }

  h1 {
    font-size: 180px;
    letter-spacing: -8px;
    margin-bottom: 0;
    color: #555 !important;
  }
`;

function Error404(props) {
  return (
    <Wrapper>
      <div className="center">
        <div>
          <h1>404</h1>
          <h2>Страница не найдена</h2>
        </div>
      </div>
    </Wrapper>
  );
}

export default Error404;
