import { createReducer } from '@reduxjs/toolkit';
import { HandbookApi } from './HandbookService';
import { hideProgress, showProgress } from '../progressBar/ProgressBarDucks';
import { localSecureStorage } from '../../_app/App';

/**
 * Constants
 */

export const handbookModul = 'handbook';
export const HANDBOOK = `${handbookModul}/HANDBOOK`;
export const SET_HANDBOOK_ITEM = `${handbookModul}/SET_HANDBOOK_ITEM`;

/**
 * Reducer
 */

const initialState = {
  handbook: {}
};

export default createReducer(initialState, {
  [HANDBOOK]: (state, action) => {
    state.handbook = action.handbook;
  },
  [SET_HANDBOOK_ITEM]: (state, { payload: { key, handbookItem } }) => {
    state.handbook = { ...state.handbook, [key]: [...state.handbook[key], handbookItem] };
  }
});

/**
 * Actions
 */

export const setHandbookItem = handbookItem => ({ type: SET_HANDBOOK_ITEM, payload: handbookItem });

export const getHandbook = () => async dispatch => {
  try {
    dispatch(showProgress());
    let { data } = await HandbookApi.getHandbook();
    data.cityOptions =
      data.city && data.city.map(({ id: value, title: label }) => ({ value, label }));
    data.foclOptions =
      data.focl && data.focl.map(({ id: value, value: label }) => ({ value, label }));
    data.locationOptions =
      data.location && data.location.map(({ id: value, value: label }) => ({ value, label }));
    data.cableNetworkTypeOptions = data['cable_network_type'].map(
      ({ id: value, title: label }) => ({
        value,
        label
      })
    );
    data.channelTypeOptions =
      data.channel_type &&
      data.channel_type.map(({ id: value, value: label }) => ({
        value,
        label
      }));
    data.baseStationChannelTypeOptions =
      data['base_station_channel_type'] &&
      data['base_station_channel_type'].map(({ id: value, value: label }) => ({
        value,
        label
      }));
    localSecureStorage.setItem('handbook', data);
    dispatch({ type: HANDBOOK, handbook: data });
    dispatch(hideProgress());
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};
