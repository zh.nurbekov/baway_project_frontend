import axios from 'axios';
import { localSecureStorage } from '../_app/App';

export const Api = axios.create({
  baseURL: '/',
  headers: {
    'Content-Type': 'application/json'
  }
});

const configureAuth = config => {
  if (!config.headers.Authorization) {
    const newConfig = {
      headers: {},
      ...config
    };
    return newConfig;
  }
  return config;
};

const unauthorizedResponse = async error => {
  if (error.response) {
    const { response } = error;
    if (error.response.status === 401 || error.response.status === 403) {
    }
  }

  return Promise.reject(error);
};

Api.interceptors.request.use(configureAuth, e => Promise.reject(e));
Api.interceptors.response.use(r => r, unauthorizedResponse);
