import React from 'react';
import Login from '../pages/login/Login';
import UserManage from '../pages/usermanage/UserManage';
import { PERMISSIONS, PERMISSIONS_TREE } from './Permissions';
import HeaderMenu from '../components/menu/HeaderMenu';
import { Icon } from 'antd';
import paths from './paths';
import Statement from '../pages/Statement/Statement';
import CreateStatement from '../pages/Statement/CreateStatement';
import Home from '../pages/Home';
import TableExample from "../pages/table/TableExample";
import Message from "../pages/message/Message";
import Board from "../pages/board/Boards";
import Info from "../pages/Info";
import Service from "../pages/Service";
import Contacts from "../pages/Contacts";

const routers = [
  {
    path: paths.home,
    title: 'Главная',
    withoutAuth: true,
    component: Home,
    exact: true
  },
  {
    path: paths.info,
    title: 'Главная',
    withoutAuth: true,
    component: Info,
    exact: true
  },
  {
    path: paths.service,
    title: 'Главная',
    withoutAuth: true,
    component: Service,
    exact: true
  },
  {
    path: paths.contacts,
    title: 'Главная',
    withoutAuth: true,
    component: Contacts,
    exact: true
  },
  {
    path: paths.login,
    title: 'Авторизация',
    component: Login,
    withoutAuth: true,
    hideSideBar: true,
    exact: true
  },

  {
    path: paths.statements,
    title: 'Заявки',
    component: Statement,
    sideBarItem: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.STATEMENTS], PERMISSIONS.STATEMENTS],
    icon: <Icon type="copy" />,
    exact: true
  },
  {
    path: paths.createStatement,
    title: 'Заявки',
    component: CreateStatement,
    withoutAuth: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.USER_MANAGE], PERMISSIONS.USER_MANAGE],
    icon: <Icon type="usergroup-delete" />,
    exact: true
  },
  {
    path: paths.workingProcess,
    title: 'Рабочий процесс',
    component: Board,
    sideBarItem: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.BOARDS], PERMISSIONS.BOARDS],
    icon: <Icon type="build" />,
    exact: true
  },
  {
    path: paths.message,
    title: 'Сообщения',
    component: Message,
    sideBarItem: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.MESSAGE], PERMISSIONS.MESSAGE],
    icon: <Icon type="message" />,
    exact: true
  },
  {
    path: paths.user,
    title: 'Администрирование',
    component: UserManage,
    withoutAuth: true,
    sideBarItem: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.USER_MANAGE], PERMISSIONS.USERS],
    icon: <Icon type="usergroup-delete" />,
    exact: true
  },
  {
    path: paths.role,
    title: 'Управление пользователями',
    component: UserManage,
    withoutAuth: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.USER_MANAGE], PERMISSIONS.ROLES],
    icon: <Icon type="usergroup-delete" />,
    exact: true
  },
  {
    path: '/table',
    title: 'Управление пользователями',
    component: TableExample,
    withoutAuth: true,
    permissions: [...PERMISSIONS_TREE[PERMISSIONS.USER_MANAGE], PERMISSIONS.USER_MANAGE],
    icon: <Icon type="usergroup-delete" />,
    exact: true
  }
];

export default routers;
