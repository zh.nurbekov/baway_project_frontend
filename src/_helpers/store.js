import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './reducers';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
const store = configureStore({
  reducer: rootReducer
  // devTools: process.env.NODE_ENV !== 'production'
});

export { store, history };
