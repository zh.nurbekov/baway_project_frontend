/**
 * Часто применяемые цвета
 */
export default {
  primary: '#5682A3',
  primaryLight: '#7a92aa',
  desc: '#666666',
  bodyBackground: '#F0F2F5',
  error: '#D32F2F',
  errorLight: '#ff8ca3',
  success: '#43A047',
  warning: '#ffa000'
};

