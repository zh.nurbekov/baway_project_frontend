const paths = {
  home: '/',
  info: '/info',
  service: '/service',
  contacts: '/contacts',
  login: '/login',
  user: '/user',
  role: '/role',
  statements: '/statements',
  createStatement: '/statement/crate',
  message: '/message',
  workingProcess: '/workingProcess',
};

export default paths;
