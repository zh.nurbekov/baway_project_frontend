import localeReducer, { localeModule } from '../components/localeProvider/LocaleDucks';
import loginReducer, { loginModule } from '../pages/login/LoginDucks';
import usersReducer, { usersModule } from '../pages/usermanage/users/UsersDucks';
import rolesReducer, { rolesModule } from '../pages/usermanage/roles/RolesDucks';
import progressReducer, { progressModule } from '../components/progressBar/ProgressBarDucks';
import handbookReducer, { handbookModul } from '../components/handbook/HandbookDucks';
import sortReducers from '../utils/sortReducers';
import statementReducer, { statementModule } from '../pages/Statement/StatementDucks';
import messageReducer,{messageModule} from "../pages/message/MessageDucks";
import boardReducer,{boardModule} from "../pages/board/BoardDucks";

export const rootReducer = sortReducers({
  [localeModule]: localeReducer,
  [loginModule]: loginReducer,
  [usersModule]: usersReducer,
  [rolesModule]: rolesReducer,
  [progressModule]: progressReducer,
  [handbookModul]: handbookReducer,
  [statementModule]: statementReducer,
  [messageModule]: messageReducer,
  [boardModule]:boardReducer,
});
