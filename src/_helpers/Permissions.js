//TODO доделать константы чтобы проава везде тягулись отсюда

export const PERMISSIONS = {
  USER_MANAGE: 'userManage',
  USERS: 'users',
  USER_EDIT: 'users_edit',
  USER_DELETE: 'users_delete',
  ROLES: 'roles',
  ROLES_EDIT: 'roles_edit',
  STATEMENTS: 'statement',
  BOARDS: 'boards',
  MESSAGE: 'message'
};

export const PERMISSION_NAMES = {
  /*Раздел Базовые станции*/

  [PERMISSIONS.USER_MANAGE]: 'Раздел "Управление пользователями"',
  [PERMISSIONS.USERS]: 'Вкладка "Пользователи"',
  [PERMISSIONS.USER_EDIT]: 'Создание и редактирование пользователей',
  [PERMISSIONS.USER_DELETE]: 'Удаление пользователей',
  [PERMISSIONS.ROLES]: 'Вкладка "Роли"',
  [PERMISSIONS.ROLES_EDIT]: 'Создание и редактирование роли',
  [PERMISSIONS.STATEMENTS]: 'Заявки',
  [PERMISSIONS.BOARDS]: 'Раздел "Рабочий процесс"',
  [PERMISSIONS.MESSAGE]: 'Раздел "Сообщения"'
};

export const PERMISSIONS_TREE = {
  [PERMISSIONS.USER_MANAGE]: [
    PERMISSIONS.USERS,
    PERMISSIONS.USER_EDIT,
    PERMISSIONS.USER_DELETE,
    PERMISSIONS.ROLES,
    PERMISSIONS.ROLES_EDIT
  ],
  [PERMISSIONS.STATEMENTS]: [],
  [PERMISSIONS.BOARDS]: [],
  [PERMISSIONS.MESSAGE]: [],
};
