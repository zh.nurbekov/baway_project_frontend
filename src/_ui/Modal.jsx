import React from 'react';
import { Modal as AntModal } from 'antd';

const Modal = ({ title, onCancel, children, width, footer = false, style, visible = true }) => {
  return (
    <AntModal
      width={width}
      // zIndex={1999}
      maskClosable={false}
      title={title}
      centered
      onCancel={onCancel}
      visible={visible}
      children={children}
      footer={footer}
      style={style}
    />
  );
};

export default Modal;
