import React from 'react';
import { InputNumber, Input as AntInput } from 'antd';
import Field from './Field';
import ValidBox from './ValidBox';

function Input({ name, style, label, type = 'text', validation, ...restProps }) {
  return (
    <div style={{ width: '100%', ...style }}>
      <Field name={name}>
        {({ onChange, error, helperText, ...fieldProps }) => {
          return (
            <ValidBox status={error ? 'error' : ''} msg={helperText}>
              <div children={label} className="mb1" />
              {type === 'number' ? (
                <InputNumber
                  type={type}
                  style={{ width: '100%' }}
                  onChange={value => {
                    value === null ? onChange('') : onChange(value);
                  }}
                  {...fieldProps}
                  {...restProps}
                />
              ) : (
                <AntInput
                  size={'large'}
                  type={type}
                  style={{ width: '100%' }}
                  onChange={({ target: { value } }) => {
                    validations(validation, value) && onChange(value);
                  }}
                  {...fieldProps}
                  {...restProps}
                />
              )}
            </ValidBox>
          );
        }}
      </Field>
    </div>
  );
}

function validations(validation, value) {
  return validation ? !!validation.test(value) : true;
}

export default Input;
