import React from 'react';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import { errorMsg } from '../../utils/errorMsg';
import { Switch as AntSwitch } from 'antd';

import ValidBox from './ValidBox';

export default class Switch extends React.Component {
  onChange = (checked, object) => {
    object.form.setFieldValue(object.field.name, checked);
  };

  render() {
    const { name, label, disabled } = this.props;
    return (
      <Field
        name={name}
        render={object => {
          let error = errorMsg(name, object.form);
          return (
            <div style={{ width: '100%' }}>
              {label && <div>{label}</div>}
              <div>
                <ValidBox status={error ? 'error' : ''} msg={error}>
                  <div>
                    <AntSwitch
                      disabled={disabled}
                      name={name}
                      checked={object.field.value}
                      onChange={checked => this.onChange(checked, object)}
                    />
                  </div>
                </ValidBox>
              </div>
            </div>
          );
        }}
      />
    );
  }
}

Switch.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func
};
