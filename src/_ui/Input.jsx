import React, { useEffect, useState } from 'react';
import { Input as InputField } from 'antd';

function Input({
  name,
  lengthToFind = 3,
  width,
  inputValue,
  onChange,
  textType = 'text',
  placeholder = 'Поиск'
}) {
  const [timeOutId, registerTimeOut] = useState(null);
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(inputValue || '');
  }, [inputValue]);

  const handleChange = ({ target: { value } }) => {
    if (textType && value.length <= 2) {
      if (value) {
        clearTimeout(timeOutId);
        onChange(null);
      }
      setValue(value);
    } else {
      clearTimeout(timeOutId);
      registerTimeOut(
        setTimeout(() => {
          onChange(value === '' ? null : value);
        }, 1000)
      );
      setValue(value);
    }
  };

  return (
    <div>
      <InputField
        value={value}
        style={{ width: width || 220 }}
        placeholder={placeholder}
        onChange={handleChange}
      />
      {textType === 'text' && value.length <= 2 && value.length > 0 && (
        <div style={{ color: 'red' }}>{'минимум 3 символа'}</div>
      )}
    </div>
  );
}

export default Input;
