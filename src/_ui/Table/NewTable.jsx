import React from 'react';
import { Pagination } from 'antd';
import ReactTable from 'react-table';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Wrapper, Loading } from './TableStyles';
import usePaginationFilter from '../../components/filters/usePaginationFilter';
import Select from "../Select";


const getPaginateOptions = () => {
  return [10, 20, 30, 50, 100].map(val => ({value: val, label: `${val} / page`}))
}

const Table = props => {
  const { data, columns, hidePagination, index = '', loadData, total } = props;
  const { pageParam, filter, changePaginationPage, changePaginationLimit } = usePaginationFilter(data, index, loadData, total);

  const customComponents = () => {
    const { onClickRow, loading } = props;
    return {
      getTrProps: (state, rowInfo, column, instance, expanded) => {
        return {
          onClick(e) {
            if (typeof onClickRow === 'function')
              onClickRow(rowInfo, e, column, instance, expanded, state);
          },
          style: {
            cursor: typeof onClickRow === 'function' ? 'pointer' : 'default'
          }
        };
      },

      getTdProps: (tableState, rowInfo, column) => {
        const { onClickTd } = props;
        return {
          onClick(e) {
            if (typeof onClickTd === 'function') onClickTd(tableState, rowInfo, column, e);
          },
          style: {
            cursor: typeof onClickTd === 'function' ? 'pointer' : 'default'
          }
        };
      },

      defaultFilterMethod: (filter, row) =>
        String(row[filter.id])
          .toLowerCase()
          .indexOf(filter.value.toLowerCase()) !== -1,

      PaginationComponent: param => {
        if (!total) {
          return null
        }
        return (
            <div className="rt-pagination flex justify-end">
              <div>
                <Pagination
                    current={pageParam.page + 1}
                    pageSize={pageParam.size}
                    onChange={page => {
                      changePaginationPage(page - 1);
                      param.onPageChange(page - 1);
                    }}
                    total={props.total}
                />
              </div>
              <div>
                <Select
                    variant="outlined" value={filter.limit}
                    options={getPaginateOptions()}
                    withoutForm={true}
                    allowClear={false}
                    style={{marginLeft: 10}}
                    onChange={(value) => {
                      changePaginationLimit(value)
                    }}
                />
              </div>
            </div>
        )
      },

      NoDataComponent: () => {
        if (loading) return null;
        else
          return (
            <Loading className="-loading -active">
              <div className="-loading-inner">Нет данных</div>
            </Loading>
          );
      },
      LoadingComponent: data => {
        if (!data.loading) return null;
        else
          return (
            <Loading className="-loading -active">
              <div className="-loading-inner">
                <div className="rt-loading-circle">
                  <CircularProgress />
                </div>
              </div>
            </Loading>
          );
      }
    };
  };

  const getData = () => {
    let i = pageParam.page * pageParam.size;
    return [
      ...data.map(item => {
        i++;
        return { ...item, rowNum: i };
      })
    ];
  };

  return (
    <Wrapper pointer={typeof props.onClickRow === 'function'}>
      <ReactTable
        {...props}
        {...customComponents()}
        columns={columns}
        data={getData()}
        pageSize={data.length || 2}
        showPagination={!hidePagination}
        sortable={false}
        className="-highlight"
      />
    </Wrapper>
  );
};

Table.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  onClickRow: PropTypes.func,
  loadData: PropTypes.func,
  total: PropTypes.number,
  index: PropTypes.number,
  hidePagination: PropTypes.bool
};

export default Table;
