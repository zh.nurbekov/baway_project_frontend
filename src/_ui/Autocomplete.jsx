import React, { useCallback } from 'react';
import { AutoComplete, Select as AntSelect } from 'antd';
import Field from './form/Field';
import ValidBox from './form/ValidBox';
import PropTypes from 'prop-types';
import Spin from 'antd/lib/spin';
import Empty from 'antd/lib/empty';

const { Option } = AutoComplete;

const getOptions = data => {
  if (!data) {
    return [];
  }
  return data.map(item => ({
    label: item.name,
    value: item.id
  }));
};

const getApiFilter = ({ search_text }) => {
  if (!search_text) {
    return {};
  }
  return { search_text };
};

let localFilter = {};
function AutocompleteComponent({
  onChange,
  loadDataApi,
  loadDataFilter,
  searchLength,
  label,
  allowClear,
  showArrow,
  showSearch,
  style,
  width,
  placeholder,
  convertOptions,
  renderOptions,
  ...otherProps
}) {
  let timer = null;
  const [loading, setLoading] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [value, setValue] = React.useState(otherProps.value || []);
  const [searchText, setSearchText] = React.useState('');

  React.useEffect(() => {
    JSON.stringify(localFilter) !== JSON.stringify(loadDataFilter) &&
      (async () => {
        setLoading(true);
        setOptions([]);
        const {
          data: { data }
        } = await loadDataApi({ ...loadDataFilter, ...getApiFilter({ search_text: searchText }) });
        const newOptions = convertOptions ? convertOptions(data) : getOptions(data);
        setOptions(newOptions);
        setLoading(false);
        localFilter = loadDataFilter;
      })();
  }, [loadDataFilter]);

  React.useEffect(() => {
    JSON.stringify(localFilter) === JSON.stringify(loadDataFilter) &&
      (async () => {
        setLoading(true);
        setOptions([]);
        const {
          data: { data }
        } = await loadDataApi({ ...localFilter, ...getApiFilter({ search_text: searchText }) });
        const newOptions = convertOptions ? convertOptions(data) : getOptions(data);
        setOptions(newOptions);
        setLoading(false);
      })();
  }, [searchText]);

  const handleChange = value => {
    setValue(value);
    if (onChange) {
      onChange(value);
    }
  };

  const handleSearch = searchValue => {
    clearTimeout(timer);
    if (searchValue.length >= searchLength) {
      timer = setTimeout(() => setSearchText(searchValue), 1000);
    } else {
      timer = setTimeout(() => searchText && setSearchText(''), 1000);
    }
  };

  return (
    <div style={{ ...style, width: width }}>
      <div children={label} />
      <AntSelect
        style={{ width: '100%' }}
        allowClear={allowClear}
        showArrow={showArrow}
        showSearch={showSearch}
        value={value}
        onChange={handleChange}
        onSearch={handleSearch}
        filterOption={false}
        notFoundContent={loading ? <Spin /> : <Empty />}
        placeholder={placeholder}
        {...otherProps}
      >
        {renderOptions
          ? renderOptions(options)
          : options.map(({ value, label }) => (
              <Option key={value} value={value}>
                {label}
              </Option>
            ))}
      </AntSelect>
    </div>
  );
}

const Autocomplete = props => {
  const { name, onChange, withoutForm, ...restProps } = props;
  return withoutForm ? (
    <AutocompleteComponent name={name} onChange={onChange} {...restProps} />
  ) : (
    <>
      <Field name={name}>
        {({ onChange, error, helperText, ...fieldProps }) => {
          return (
            <ValidBox status={error ? 'error' : ''} msg={helperText}>
              <AutocompleteComponent
                name={name}
                onChange={onChange}
                {...restProps}
                {...fieldProps}
              />
            </ValidBox>
          );
        }}
      </Field>
    </>
  );
};

Autocomplete.propsTypes = {
  withoutForm: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  searchLength: PropTypes.string,
  placeholder: PropTypes.string,
  allowClear: PropTypes.bool,
  showArrow: PropTypes.bool,
  showSearch: PropTypes.bool
};

Autocomplete.defaultProps = {
  withoutForm: false,
  searchLength: 3,
  allowClear: true,
  showArrow: false,
  showSearch: true
};

export default Autocomplete;
