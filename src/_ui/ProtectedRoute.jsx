import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import moment from 'moment';
import { logout } from '../pages/login/LoginDucks';
import {localSecureStorage} from "../_app/App";


const ProtectedRoute = ({ component: Component, ...rest }) => {
  const dispatch = useDispatch();
  return (
    <Route
      {...rest}
      render={routeProps => {
        // let user = localSecureStorage.getItem('user');
        // let expiredAt = localSecureStorage.getItem('expiredAt');
        //
        // if (rest.withoutAuth && user === null) {
        //   return <Redirect to="/login" />;
        // }
        //
        // if (expiredAt && expiredAt - moment(new Date()).unix() < 0) {
        //   dispatch(logout());
        //   return;
        // }
        //
        // if (user && rest.path === '/login') {
        //   return <Redirect to="/" />;
        // }

        return <Component {...routeProps} />;
      }}
    />
  );
};

export default connect()(ProtectedRoute);
