import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import ValidBox from './form/ValidBox';
import Field from './form/Field';
import { Select as AntSelect } from 'antd';
const { Option } = AntSelect;

function SelectComponent({ showSearch, value, onChange, options, style, width, label, ...props }) {
  return (
    <div style={{ ...style, width: width }}>
      <div children={label} className="mb1" />
      <AntSelect
        size={'large'}
        style={{ width: '100%', borderRadius: 8 }}
        allowClear={props.allowClear}
        showSearch={showSearch}
        value={value || ''}
        optionFilterProp="children"
        onChange={value => onChange(value)}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        {...props}
      >
        {options.map(({ value, name }) => (
          <Option key={value} value={value}>
            {name}
          </Option>
        ))}
      </AntSelect>
    </div>
  );
}

const Select = props => {
  const { name, onChange, withoutForm, ...restProps } = props;
  return withoutForm ? (
    <SelectComponent name={name} onChange={onChange} {...restProps} />
  ) : (
    <>
      <Field name={name}>
        {({ onChange, error, helperText, ...fieldProps }) => {
          return (
            <ValidBox status={error ? 'error' : ''} msg={helperText}>
              <SelectComponent name={name} onChange={onChange} {...restProps} {...fieldProps} />
            </ValidBox>
          );
        }}
      </Field>
    </>
  );
};

Select.propsTypes = {
  withoutForm: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired
};

Select.defaultProps = {
  withoutForm: false
};

export default Select;
