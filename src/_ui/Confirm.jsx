import React from 'react';
import { Popconfirm } from 'antd';

export default function Confirm({
  children,
  onConfirm,
  title = '',
  okText = 'Да',
  cancelText = 'Отмена'
}) {
  return (
    <Popconfirm
      zIndex={1999}
      placement="top"
      title={title}
      okText={okText}
      cancelText={cancelText}
      onConfirm={e => {
        onConfirm(e);
        e.stopPropagation();
      }}
      children={children}
      onCancel={e => e.stopPropagation()}
      style={{ zIndex: 2000 }}
    />
  );
}
