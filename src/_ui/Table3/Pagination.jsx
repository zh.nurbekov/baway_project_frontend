import React from 'react';
import { ListItemText, MenuItem, Select } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Pagination as MuiPagination } from '@material-ui/lab';

const useStyles = makeStyles(() => ({
  root: {
    '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
      transform: 'translate(0px, 0px) scale(0.75) !important'
    },
    '& .MuiInputLabel-outlined': {
      transform: 'translate(0, 24px) scale(1) !important'
    },
    '& .MuiOutlinedInput-input': {
      padding: '0px 27px 0px'
    }
  }
}));

const getPaginateOptions = () => {
  return [10, 20, 30, 50, 100].map(val => ({ value: val, name: val }));
};

export default function Pagination({
  total,
  offset,
  data,
  limit,
  page,
  setLimit,
  setOffset,
  setPage
}) {
  const classes = useStyles();

  if (!total) {
    return null;
  }
  const visibleCount =
    offset + data.length > offset + limit ? offset + limit : offset + data.length;
  const totalVisible = total <= visibleCount ? total : limit + offset;
  return (
    <div className="rt-pagination items-center pb1">
      <div className="center mr2 items-center">
        Показаны записи {1 + offset} - {totalVisible} из {total}
      </div>
      <div>
        <Select
          className={classes.root}
          inputProps={{ padding: 0 }}
          variant="outlined"
          value={limit}
          onChange={event => {
            setLimit(event.target.value);
            setOffset(0);
            setPage(1);
          }}
        >
          {getPaginateOptions().map(({ value, name }) => {
            return (
              <MenuItem value={value} key={value}>
                <ListItemText primary={name} />
              </MenuItem>
            );
          })}
        </Select>
      </div>
      <MuiPagination
        page={page}
        count={total / limit < 1 ? 1 : Math.ceil(total / limit)}
        onChange={(event, value) => {
          const newOffset = value === 0 ? (value + 1) * limit : (value - 1) * limit;
          setOffset(newOffset);
          setPage(value);
        }}
      />
    </div>
  );
}
