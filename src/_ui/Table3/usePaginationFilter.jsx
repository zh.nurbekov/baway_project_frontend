import { useState, useEffect } from 'react';

const paginateOptions = [10, 20, 30, 50, 100];

function usePaginationFilter(filters, loadData, total, data) {
  const [limit, setLimit] = useState(paginateOptions[0]);
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState(1);
  const [localFilter, setLocalFilter] = useState({});
  const [totalFilter, setTotalFilter] = useState({});
  const [localTotal, setLocalTotal] = useState(0);
  const [timeOutId, registerTimeOut] = useState(null);

  useEffect(() => {
    onTotalChange();
  }, [total]);

  useEffect(() => {
    onOffsetChange();
  }, [limit, offset]);

  useEffect(() => {
    onFilterChange();
  }, [filters]);

  const onTotalChange = () => {
    const isFilterNotChanged = JSON.stringify(totalFilter) === JSON.stringify(filters);
    const doUpdatePage = limit - 1 < total;
    const isNotLastPage = limit - data.length === 1;
    if (doUpdatePage && isFilterNotChanged) {
      const increaseTotal = localTotal - total === -1 && limit < total;
      const decreaseTotal = total - localTotal === -1 && isNotLastPage;
      const isFirstItem = localTotal === 0;
      const lastItem = !data.length;
      if (!isFirstItem && (increaseTotal || decreaseTotal) && !lastItem) {
        loadData(filters);
      }
      if (lastItem) {
        setOffset(offset - limit);
        setPage(page - 1);
      }
    }
    setLocalTotal(total);
    setTotalFilter(filters);
  };

  const onOffsetChange = () => {
    const newFilter = { ...filters, limit, offset };
    const isOffsetChange =
      localFilter && (localFilter.offset !== offset || localFilter.limit !== limit);
    if (isOffsetChange) {
      loadData(newFilter);
      setLocalFilter(newFilter);
    }
    setLocalTotal(total);
    setTotalFilter(newFilter);
  };

  const onFilterChange = () => {
    const changeHandle = () => {
      const isFilterChanged = JSON.stringify(localFilter) !== JSON.stringify(filters);
      if (isFilterChanged) {
        const newFilter = { ...filters, limit, offset: 0 };
        setLocalFilter(newFilter);
        loadData(newFilter);
        setOffset(0);
        setPage(1);
      }
    };
    clearTimeout(timeOutId);
    registerTimeOut(
      setTimeout(() => {
        changeHandle();
      }, 500)
    );
    setLocalTotal(total);
  };

  return { page, setPage, limit, setLimit, offset, setOffset };
}

export default usePaginationFilter;
