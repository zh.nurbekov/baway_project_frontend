import React from 'react';

/**
 * queryDefaultParam - дополнительные параметры для queryFilter
 * */

const withTable = Component => {
  return class extends React.Component {
    componentWillUnmount() {
      const { history, location, queryFilterName } = this.props;
      if (window.location.search && window.location.pathname === location.pathname) {
        const filterName = `filter${queryFilterName}`;
        const params = new URLSearchParams(location.search);
        params.set(filterName, JSON.stringify({}));
        history.replace(`${location.pathname}?${params.toString()}`);
      }
    }

    getQueryFilter() {
      const { location, queryFilterName } = this.props;
      const filterName = `filter${queryFilterName}`;
      const params = new URLSearchParams(location.search);
      const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : {};
      return filter;
    }

    changeQueryFilter(filter) {
      const { history, location, queryFilterName, loadData } = this.props;
      const filterName = `filter${queryFilterName}`;
      const params = new URLSearchParams(location.search);
      params.set(filterName, JSON.stringify(filter));
      history.replace(`${location.pathname}?${params.toString()}`);
      loadData && loadData(filter);
    }

    render() {
      const { loadData, filters, queryDefaultParam } = this.props;
      const queryFilter = { ...queryDefaultParam, ...this.getQueryFilter() };
      const changeFilter = filter => {
        filters ? loadData(filter) : this.changeQueryFilter(filter);
      };

      return <Component {...this.props} filters={filters || queryFilter} loadData={changeFilter} />;
    }
  };
};

export default withTable;
